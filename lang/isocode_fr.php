<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/nomenclatures.git

return [

	// D
	'desc_nomenclature_iana5646subtags' => 'Le site IANA fournit un certains nombre de ressources dont un ensemble de registre sur les protocoles informatiques. L’un d’entre eux est le registre des subtags 5646, c’est-à-dire la liste officielle des sous-étiquettes qui composent les étiquettes de langue utilisées dans les protocoles informatiques.',

	// G
	'geometrie_beprovince' => 'Contours des provinces belges, ULg & IGN',
	'geometrie_beregion' => 'Contours des régions belges, ULg & IGN',
	'geometrie_chcanton' => 'Contours des cantons suisses, Swisstopo',
	'geometrie_continents' => 'Contours des continents, boB Rudis',
	'geometrie_countries' => 'Contours des pays au 1:50m, Natural Earth',
	'geometrie_deland' => 'Contours des lander allemands, BKG',
	'geometrie_frdepartment' => 'Contours des départements français, IGN',
	'geometrie_frpnmpark' => 'Contours des parcs naturels marins français',
	'geometrie_frpnpark' => 'Contours des parcs nationaux français',
	'geometrie_frpnrpark' => 'Contours des parcs naturels régionaux français',
	'geometrie_frregion' => 'Contours des régions françaises, IGN',
	'geometrie_nlprovince' => 'Contours des provinces néerlandaises, Kadaster',
	'geometrie_ptdistrict' => 'Contours des districts portugais, Direção-Geral do Território',
	'geometrie_usstate' => 'Contours des états américains, Census Bureau',

	// I
	'infrasubdivision_inseearrond' => 'Arrondissements français selon l’INSEE',
	'infrasubdivision_inseecommune' => 'Communes et arrondissements municipaux français selon l’INSEE',
	'infrasubdivision_inseeepci' => 'EPCI français selon l’INSEE',
	'infrasubdivision_uscbcounty' => 'Comtés et entités équivalentes selon le Census Bureau',

	// L
	'label_feed_category_default' => 'Nomenclatures diverses',
	'label_feed_category_language' => 'Nomenclatures des langues',
	'label_feed_category_map' => 'Contours des territoires',
	'label_feed_category_map_pa' => 'Contours des zones protégées',
	'label_feed_category_territory' => 'Nomenclatures des territoires',

	// M
	'menu_isocode' => 'Nomenclatures officielles',

	// N
	'nomenclature_geoipcontinents' => 'Codes GeoIP des continents',
	'nomenclature_iana5646subtags' => 'Sous-étiquettes de langues RFC 5646',
	'nomenclature_iso15924scripts' => 'Indicatifs d’écritures ISO 15924',
	'nomenclature_iso3166alternates' => 'Codes alternatifs des subdivisions',
	'nomenclature_iso3166countries' => 'Indicatifs des pays ISO 3166-1',
	'nomenclature_iso3166subdivisions' => 'Indicatifs des subdivisions des pays ISO 3166-2',
	'nomenclature_iso4217currencies' => 'Devises ISO 4217',
	'nomenclature_iso639codes' => 'Codes de langue ISO 639-1, 2 et 3',
	'nomenclature_iso639families' => 'Familles et groupes de langues ISO 639-5',
	'nomenclature_iso639macros' => 'Macrolangues',
	'nomenclature_iso639names' => 'Noms de langues',
	'nomenclature_iso639retirements' => 'Langues supprimées',
	'nomenclature_m49regions' => 'Indicatifs des régions du monde selon l’UN M.49',
	'nomenclature_protected_areas' => 'Nomenclature des zones protégées',

	// T
	'titre_page_isocode' => '@plugin@ - Gestion des feeds',
];
