<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/nomenclatures.git

return [

	// I
	'isocode_description' => 'Ce plugin installe la base de divers codes standard comme les codes de langues ISO-639, les codes d’écritures ISO-15924, les codes standard des territoires, etc. Il inclut aussi une base de contours GIS pour une liste de territoires.
	 Le plugin fournit aussi une API REST pour la consultation de ces données par des plugins utilisateur comme Territoires.',
	'isocode_nom' => 'Nomenclatures officielles',
	'isocode_slogan' => 'Nomenclatures pour les langues, les territoires et autres éléments normalisés',
];
