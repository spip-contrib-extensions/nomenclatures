<?php
/**
 * Ce fichier contient l'ensemble des fonctions implémentant l'API du plugin.
 *
 * @package SPIP\ISOCODE\API
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Retourne la liste des identifiants des tables utilisées par les feeds du plugin.
 * Il est possible de filtrer cette liste selon des critères issus des champs des feeds.
 *
 * @api
 *
 * @uses feed_repertorier()
 *
 * @param null|array $filtres Tableau associatif `[champ] = valeur` de critères de filtres sur les descriptions de feed.
 *                            Les opérateurs possibles sont l'égalité et la non égalité.
 *
 * @return array Liste des tables sans le préfixe `spip_`.
 */
function feed_repertorier_tables(?array $filtres = []) : array {
	// On filtre toujours la liste des feeds sur ceux dont la cible est une table SQL.
	include_spip('inc/ezmashup_feed');
	$filtres['target_format'] = 'sql_table';
	$feeds = feed_repertorier('isocode', $filtres);

	// On constitue la liste des tables
	$tables = [];
	foreach ($feeds as $_feed) {
		$tables[] = $_feed['target_id'];
	}

	// Eviter les doublons
	return array_unique($tables);
}

/**
 * Compile les hash des feeds ayant remplis les données de la colleciton à partir de la colonne `service` (toujours
 * utilisée avec ce nom par Nomenclatures).
 *
 * @param array       $donnees Données requêtées
 * @param null|string $index   Type de hash à compiler
 *
 * @return array
 */
function feed_compiler_hash(array $donnees, ?string $index = 'hash') : array {
	$hash = [];

	// Extraire la liste des feed ayant remplis les données
	$feeds = array_unique(array_column($donnees, 'service'));

	// On boucle sur chaque feed et on récupère la consigne de son exécution pour en extraire le hash
	include_spip('inc/ezmashup_dataset_target');
	foreach ($feeds as $_id_feed) {
		$consigne = dataset_target_informer('isocode', $_id_feed);
		$hash[$_id_feed] = $consigne['records'][$index];
	}

	return $hash;
}
