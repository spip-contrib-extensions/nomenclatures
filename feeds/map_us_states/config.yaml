# L'identifiant du service est le nom du fichier YAML sans extension ou au nom du dossier
title: '<:isocode:geometrie_usstate:>'
description: ''

# Catégorisation du feed
# -- par défaut, Mashup Factory propose toujours un tag nommé category, obligatoire pour tous les feeds
category: 'map'
# -- En outre, il est possible de compléter cette catégorie par des tags utilisable ensuite dans le mapping
tags:
  type: 'subdivision'
  pays: 'US'

# Configuration du stockage de destination
target:
  # Description du tyme de stockage des données produites par le service
  # - obligatoire, prend les valeurs 'sqltable' ou 'file'
  format: 'sql_table'
  # - obligatoire, nom de la table sans spip_ ou nom du fichier avec extension.
  #   L'extension suffit à savoir comment exporter les données dans le fichier
  id: 'geoboundaries'
  # Options de paramétrage de la cible :
  # - max_chunk, permet de limiter le nombre d'enregistrements de la target à stocker en une fois (insert table SQL)
  options:
    max_chunk: 50

# Configuration du mapping et du processus de mashup
mapping:
  # Données de base : ces données sont issues d'une source primaire
  # - obligatoire
  # - désigne soit un champ directement extractible de la source soit un noeud dy type node1/node2/champ
  basic_fields:
    code: 'fields/stusab'
    geometry: 'fields/st_asgeojson'
    lat_lon: 'fields/geo_point_2d'
  # Liste des champs de base étendus : ces données sont calculées à partir des champs de base et inclus dans le stockage
  # - facultatif
  # - Le champ 'lat_lon' permet de construire les champs 'lat' et 'lon'
  extra_fields: ['lat', 'lon']
  # Données statiques : à ajouter à la target mais ne provient pas d'un dataset source
  # - falcultatif, la syntaxe indique la méthode de calcul
  #   '/field' désigne un champ de la config
  #   'valeur' désigne une valeur immédiate
  #   '$valeur' désigne la variable de contexte $type pour le type de service ou $service pour l'id du service
  static_fields:
    service: '/feed_id'
    code_type: 'code_iso'
    format: 'geojson'
    type: '#type'
    country: '#pays'
  # Liste des champs de la target qui ne seront pas retenus pour le stockage
  # - facultatif, ces champs servent au déroulement du processus uniquement (ex: les traductions)
  # - on ne retient pas le champ 'lat_lon' car il a permis de calculer les champs étendus
  unused_fields: ['lat_lon']

# Si le service nécessite des traitements spécifiques, il faut indiquer dans quel fichier PHP les trouver.
# C'est toujours le cas si le feed importe des datasets secondaires mais ce n'est pas limité à ce cas.
# Facultatif, si absent on considère qu'il n'y a pas de besoin.
# Trois formats sont autorisés:
# - '/nom_fichier' indique que le fichier est dans le dossier de personnalisation du plugin utilisateur, soit 'ezmashup/' (conseillé).
# - '//nom_fichier' indique que le fichier est dans le répertoire relatif du feed, soit 'dossier_des_feeds/id_du_feed/'.
# - 'dir_relatif/nom_fichier' ou 'nom_fichier' indique qu'il faut utiliser le chemin relatif fourni tel dans le path.
include: '/map'

# Description des datasets source primaires
# - un dataset primaire alimente les champs de base de la cible.
# - tous les datasets primaires fournissent la même liste de champs de la cible.
sources_basic:
  # Les sources primaires fournissent toujours les mêmes données suivant le même format
  # - il est donc possible de factoriser certaines configuration pour éviter la duplication
  #   Cette configuration doit être comprise comme une initialisation par défaut
  us-state-boundaries:
    # Complément pour la source
    source:
      # Prend les valeurs file, web (page html) et api
      type: 'api'
      # Définit le format du fichier ou du contenu web retourné : obligatoire.
      format: 'geojson'
      # Nom du fichier avec extension ou URL HTTP
      uri: 'https://public.opendatasoft.com/api/records/1.0/search/?dataset=us-state-boundaries&q=&rows=-1'
      last_update: '24/10/2019'
      version: ''
      license: 'Domaine public'
    # Informations optionnelles permettant de décoder le contenu de la source
    decoding:
      # - root_node : sommet de l'arborescence à extraire (sous la forme a/b/c)
      root_node: 'records'
    # Description du provider pour les crédits
    provider:
      name: 'U.S. Census Bureau'
      url: 'https://public.opendatasoft.com/explore/dataset/us-state-boundaries/information/?flg=fr'
