# Crédits pour les ressources UN M49 des régions du monde

Le fichiers M49regions.csv et M49regions_countries.csv sont construits à partir des pages :

    https://unstats.un.org/unsd/methodology/m49/
    https://unstats.un.org/unsd/methodology/m49/overview/

## Liste des codes des régions du monde

Le fichier :
* M49regions.csv

Pour le fichier `M49regions.csv`, il faut récupérer chaque table pour les 6 traductions de la page 
https://unstats.un.org/unsd/methodology/m49/, menu Geographic Regions et, extraire uniquement 
les régions (laisser les pays que l’on repère par leur code alpha3). Après on construit à la 
main la liste avec le code M49 de la région (001, 002...) et chaque traduction dans une colonne 
(les langues sont en, fr, es, ru, ar et zh).

Il faut aussi déterminer le parent à partir de l’arborescence présentée dans la page du site. La 
catégorie est mise à `zone_region` pour la plupart sauf les continents au sens GEOIP à qui ont affectent la catégorie continent.

Pour les continents, à partir du moment où l’on choisi de retenir les codes GEOIP, cela revient 
à choisir le modèle à 7 continents. Ce n’est pas le modèle que l’on vient de saisir dans le 
fichier `M49regions.csv`. Il faut donc le modifier comme le précise le renvoi en fin de tableau de 
la page des regions :

> The continent of North America (numerical code 003) comprises Northern America (numerical 
code 021), Caribbean (numerical code 029), and Central America (numerical code 013

Il faut donc faire les opérations suivantes sur le fichier :

* ajouter la ligne de la région North America / Amérique du Nord qui a bien un code M49, le 003 
et lui affecter la catégorie continent.
* lier cette région à son parent Amériques de code 019
* supprimer la région 419 qui ne va plus être utile
* lier la région Amérique du Sud 005 à son parent la région Amériques 019 étant donné que 419 a 
été supprimée et passer la catégorie à continent
* lier les régions Amérique Centrale 013, Caraïbes 029, Amérique Septentrionale 021 à son parent 
le continent Amérique du Nord 003

Les modifications de ce fichier sont assez rares et décrites dans la même page, menu Recent Changes.
