# Les ressources ISO des pays

## Les codes ISO 3166-1

La norme ISO 3166-1 fournit la nomenclature internationale des 249 pays mais comme d'habitude aucun fichier source 
gratuit. Il faut donc le construire à partir d'une page ISO, à savoir: https://www.iso.org/obp/ui/fr/#search,
lien Liste complète des noms de pays.
Le tableau présenté est paginé, il faut choisir 300 items par page de façon à l'afficher en entier. 
Le tableau affiche 5 colonnes, le nom court anglais, le nom court français et les 3 codes ISO 3166-1 alpha2, alpha3 et numérique.

On copie le tableau dans un fichier csv, en remplaçant les tabulations de séparation par un point virgule 
et on adapte le nom des colonnes. Enfin, on rajoute une colonne supplémentaire nommée "category" que l'on remplit avec 
la valeur "country" pour tous les pays.

## Les traductions additionnelles UNTERM

La base de données de terminologie de l'UN, UNTERM, permet d'exporter un fichier de traduction des pays dans les langues 
russe, chinoise, arabe et espagnole (les langues française et anglaise sont déjà connues). 
Le fichier excel d'export contient les nom court et long de chacune de ces langues. Seuls les noms courts sont utilisés.

Le problème du fichier obtenu est qu'il ne fait aucune référence à un code ISO du pays. Il faut donc construire le fichier final en utilisant ajoutant un code ISO à pays (un pays = une ligne). En outre, il n'y a que 197 lignes alors qu'il existe 259 pays.

Pour ajouter les code ISO alpha2 on crée une feuille supplémentaire avec le tableau des codes ISO des pays qui contient la traduction française et anglaise. On crée dans la feuille des traductions deux colonnes, fr et en, avec une formule en RECHERCHEV qui permet de trouver le code ISO à partir des traductions en et fr.

De fait, on remarque que quelques traductions sont différentes :
- il existe des * dans les traductions UNTERM que l'on peut supprimer
- il existe des traductions dans le fichier ISO source qui ne sont pas identiques que celles de l'UNTERM, la différence se trouvant toujours dans les termes entre parenthèses. En général, celles de l'UNTERM sont meilleures car elle intègre les mots "république de" ou "ile" dans le nom et pas dans les parenthèses. Cela est particulièrement évident avec les deux Congos.

On crée donc deux fichiers csv séparés:
- celui des traductions UNTERM des pays dont le nom fr et en est identique à l'ISO;
- celui des traductions UNTERM des pays dont le nom fr ou en est différent de l'ISO.

En outre, il existe des noms ISO qui ne possède pas de traduction UNTERM mais dont le nom ISO en ou fr pourrait suivre la même logique que l'UNTERM. On crée un troisième fichier pour ces nouvelles traductions en et fr.


## Les ressources GEONAMES

Geonames est un organisme qui fournit des données géographiques soit par le biais de fichiers CSV intégrables dans une base de données soit par l'intermédiaire de services web REST.

Dans Isocode, on utilise Geonames pour compléter la table des pays - iso3166countries - avec les informations contenues dans le fichier Geonames <code>countryInfo.txt</code> disponible à l'adresse [->http://download.geonames.org/export/dump/countryInfo.txt]. Ce fichier est au format texte de type CSV avec une tabulation comme délimiteur. Il suffit donc d'enregistrer la page précédemment citée et de retoucher le fichier en supprimant les commentaires repérés par un caractère <code>#</code> sauf la ligne précédent le premier pays qui fera office d'en-tête après lui avoir supprimé le caractère <code>#</code>.

Le fichier est in fine renommé en <code>iso3166countries-geonames-info.tab</code>.

## Association des pays et des régions

Le fichier :
* M49regions_countries.csv

Ce fichier permet de faire le lien entre chaque pays de la liste ISO 3166 et les régions M49. Il 
suffit donc de garder deux colonnes, le code M49 du pays et le code M49 de sa région parent.

Le plus simple est d’exporter le fichier Excel français de la page 
https://unstats.un.org/unsd/methodology/m49/overview/ accessible  par le menu Fullview.

On ouvre le fichier Excel et on supprime toutes le colonnes à droite de la colonne «M49 Code». 
On crée un titre «parent» dans la colonne juste à droite et on y copie la valeur de la région 
rattachée : pour la déterminer il suffit de prendre la région la plus à droite parmi les 4 
niveaux d’arborescence. Une fois fini, on peut supprimer les colonnes de région (8 colonnes) 
ainsi que la colonne du nom français du pays qui ne nous sert pas.

Pour obtenir 015 au lieu de 15, on duplique les colonnes «M49 Code» et «Parent» avec la formule 
`TEXTE(cellule, "000")` que l’on recopie ensuite en copie valeur dans les colonnes «M49 Code» et 
«Parent». On peut alors supprimer les colonnes de calcul et renommer la colonne «M49 Code» en «code_num».

Enfin, il faut modifier le code_num du pays Antartique de 001 (monde) à 010 qui est le code du 
continent Antartique (on a donc deux territoires qui ont le même code mais pas utilisés en même temps).

Pour terminer, on enregistre le fichier Excel en CSV avec un «;» en guise de séparateur en le 
nommant `M49regions_countries.csv`.

Ce fichier varie peu mais surement plus que celui des régions. Il est facile de le reconstruire 
si on constate une modification listée dans la page dédiée.

Il est aussi à noter que Taiwan n’apparait pas comme un pays UN M49.