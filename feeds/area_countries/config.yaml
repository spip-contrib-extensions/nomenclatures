# L'identifiant du service est le nom du fichier YAML sans extension ou au nom du dossier
title: '<:isocode:nomenclature_iso3166countries:>'
description: ''

# Catégorisation du feed
# -- par défaut, Mashup Factory propose toujours un tag nommé category, obligatoire pour tous les feeds
category: 'territory'
tags:
  type: 'country'

# Configuration du stockage de destination
target:
  # Description du tyme de stockage des données produites par le service
  # - obligatoire, prend les valeurs 'sqltable' ou 'file'
  format: 'sql_table'
  # - obligatoire, nom de la table sans spip_ ou nom du fichier avec extension.
  #   L'extension suffit à savoir comment exporter les données dans le fichier
  id: 'iso3166countries'

# Configuration du mapping et du processus de mashup
mapping:
  # Données de base : ces données sont issues d'une source primaire
  # - obligatoire
  # - désigne soit un champ directement extractible de la source soit un noeud dy type node1/node2/champ
  basic_fields:
    label_en: 'English name'
    label_fr: 'French name'
    code_alpha2: 'Alpha-2'
    code_alpha3: 'Alpha-3'
    code_num: 'Numeric'
    category: 'category'
  static_fields:
    service: '/feed_id'
  # Champs supplémentaires fournies par des sources secondaires
  # - facultatif, un mapping par dataset avec un format identifique aux basic_fields
  addon_fields:
    geonames_infos:
      capital: 'Capital'
      area: 'Area(in sq km)'
      population: 'Population'
      code_continent: 'Continent'
      tld: 'tld'
      code_4217_3: 'CurrencyCode'
      currency_en: 'CurrencyName'
      phone_id: 'Phone'
    m49region_links:
      code_num_region: 'parent'
    trad_unterm_iso:
      label: ''
    trad_unterm_add:
      label: ''
    trad_unterm_dif:
      label: ''
  # Indique qu'il existe un champ 'label' de type multi
  # - facultatif
  has_label_field: 'pre'
  # Liste des champs de la target qui ne seront pas retenus pour le stockage
  # - facultatif, ces champs servent au déroulement du processus uniquement (ex: les traductions)
  unused_fields: ['label_en', 'label_fr']

# Si le service nécessite des traitements spécifiques, il faut indiquer dans quel fichier PHP les trouver.
# C'est toujours le cas si le feed importe des datasets secondaires mais ce n'est pas limité à ce cas.
# Facultatif, si absent on considère qu'il n'y a pas de besoin.
# Trois formats sont autorisés:
# - '/nom_fichier' indique que le fichier est dans le dossier de personnalisation du plugin utilisateur, soit 'ezmashup/' (conseillé).
# - '//nom_fichier' indique que le fichier est dans le répertoire relatif du feed, soit 'dossier_des_feeds/id_du_feed/'.
# - 'dir_relatif/nom_fichier' ou 'nom_fichier' indique qu'il faut utiliser le chemin relatif fourni tel dans le path.
include: '/area'

# Description des datasets source primaires
# - un dataset primaire alimente les champs de base de la cible.
# - tous les datasets primaires fournissent la même liste de champs de la cible.
sources_basic:
  # Les sources primaires fournissent toujours les mêmes données suivant le même format
  # - il est donc possible de factoriser certaines configuration pour éviter la duplication
  #   Cette configuration doit être comprise comme une initialisation par défaut
  iso3166_countries:
    # Complément pour la source
    source:
      # Prend les valeurs file, web (page html) et api : obligatoire
      type: 'file'
      # Définit le format du fichier ou du contenu web retourné : obligatoire.
      format: 'csv'
      # Nom du fichier avec extension ou URL HTTP
      uri: 'iso3166countries.csv'
      last_update: '2022'
      version: ''
      license: ''
    decoding:
      # - delimiter : delimiteur pour un fichier csv
      delimiter: ';'
    # Description du provider pour les crédits
    provider:
      name: 'ISO'
      url: 'https://www.iso.org/home.html'
    # Description de la source externe fichier dont est issue la source locale
    build_from:
      # Nom du fichier dont est issue la source locale. Cette section est vide si la source est une api ou page web
      file: 'https://www.iso.org/obp/ui/fr/#search'

# Description des datasets source secondaires
# - un dataset secondaire ajoute des champs complémentaires une fois que les champs primaires ont été extraits
sources_addon:
  geonames_infos:
    source:
      # Prend les valeurs file, web (page html) et api
      type: 'file'
      # Prend les valeurs csv (delimiter virgule), tsv (delimiter tab), ssv (delimiter point-virgule), json, geojson, xml, html
      format: 'csv'
      # Nom du fichier avec extension ou URL HTTP
      uri: 'iso3166countries-geonames-info.tab'
      # Dernière mise à jour de la source
      last_update: '2022'
      version: ''
      license: ''
    decoding:
      # - delimiter : delimiteur pour un fichier csv
      delimiter: "\t"
    provider:
      name: 'Geonames'
      url: 'https://www.geonames.org/'
    # Description de la source externe fichier dont est issue la source locale
    build_from:
      # Nom du fichier dont est issue la source locale. Cette section est vide si la source est une api ou page web
      file: 'http://download.geonames.org/export/dump/countryInfo.txt'
  m49region_links:
    source:
      # Prend les valeurs file, web (page html) et api
      type: 'file'
      # Prend les valeurs csv (delimiter virgule), tsv (delimiter tab), ssv (delimiter point-virgule), json, geojson, xml, html
      format: 'csv'
      # Nom du fichier avec extension ou URL HTTP
      uri: 'm49regions_countries.csv'
      # Dernière mise à jour de la source
      last_update: '2022'
      version: ''
      license: ''
    decoding:
      # - delimiter : delimiteur pour un fichier csv
      delimiter: ';'
    provider:
      name: 'United Nations - Statistics Division'
      url: 'https://unstats.un.org/UNSDWebsite/'
    # Description de la source externe fichier dont est issue la source locale
    build_from:
      # Nom du fichier dont est issue la source locale. Cette section est vide si la source est une api ou page web
      file: 'https://unstats.un.org/unsd/methodology/m49/overview/'
  trad_unterm_iso:
    source:
      # Prend les valeurs file, web (page html) et api
      type: 'file'
      # Prend les valeurs csv (delimiter virgule), tsv (delimiter tab), ssv (delimiter point-virgule), json, geojson, xml, html
      format: 'csv'
      # Nom du fichier avec extension ou URL HTTP
      uri: 'iso3166countries-trad-unterm-iso.csv'
      # Dernière mise à jour de la source
      last_update: '2022'
      version: ''
      license: ''
    decoding:
      # - delimiter : delimiteur pour un fichier csv
      delimiter: ';'
    provider:
      name: 'United Nations - Terminology Database'
      url: 'https://unterm.un.org/unterm2/en/'
    # Description de la source externe fichier dont est issue la source locale
    build_from:
      # Nom du fichier dont est issue la source locale. Cette section est vide si la source est une api ou page web
      file: 'https://unterm.un.org/unterm2/en/country'
  trad_unterm_add:
    source:
      # Prend les valeurs file, web (page html) et api
      type: 'file'
      # Prend les valeurs csv (delimiter virgule), tsv (delimiter tab), ssv (delimiter point-virgule), json, geojson, xml, html
      format: 'csv'
      # Nom du fichier avec extension ou URL HTTP
      uri: 'iso3166countries-trad-unterm-add.csv'
      # Dernière mise à jour de la source
      last_update: '2022'
      version: ''
      license: ''
    decoding:
      # - delimiter : delimiteur pour un fichier csv
      delimiter: ';'
    provider:
      name: 'United Nations - Terminology Database'
      url: 'https://unterm.un.org/unterm2/en/'
    # Description de la source externe fichier dont est issue la source locale
    build_from:
      # Nom du fichier dont est issue la source locale. Cette section est vide si la source est une api ou page web
      file: 'https://unterm.un.org/unterm2/en/country'
  trad_unterm_dif:
    source:
      # Prend les valeurs file, web (page html) et api
      type: 'file'
      # Prend les valeurs csv (delimiter virgule), tsv (delimiter tab), ssv (delimiter point-virgule), json, geojson, xml, html
      format: 'csv'
      # Nom du fichier avec extension ou URL HTTP
      uri: 'iso3166countries-trad-unterm-dif.csv'
      # Dernière mise à jour de la source
      last_update: '2022'
      version: ''
      license: ''
    decoding:
      # - delimiter : delimiteur pour un fichier csv
      delimiter: ';'
    provider:
      name: 'United Nations - Terminology Database'
      url: 'https://unterm.un.org/unterm2/en/'
    # Description de la source externe fichier dont est issue la source locale
    build_from:
      # Nom du fichier dont est issue la source locale. Cette section est vide si la source est une api ou page web
      file: 'https://unterm.un.org/unterm2/en/country'
