# Crédits pour les infrasubdivisions

Les tables sont fournies via des fichiers CSV (sépateur ";" ou ",").


## FR - Liste des EPCI selon l'INSEE

Le fichier :

* `inseeepci.csv`

Le fichier est construit à partir du source de l'INSEE disponible sur la page [Intercommunalité](https://www.insee.fr/fr/information/2510634) et plus précisément des onglets "EPCI" et "Composition_communale".

A partir de l'onglet EPCI on ajoute une colonne DEP par une rechercheV en recherchant dans
l'onglet "Composition_communale". Ensuite, on supprime la colonne NB_COM et on exporte l'onglet
"EPCI" en fichier en CSV avec le séparateur point-virgule et que l'on renomme comme indiqué.

Si l'export ne fonctionne pas avec Excel (caractères non UTF) alors utiliser Numbers en copiant le contenu.
