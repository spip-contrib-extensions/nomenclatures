# Crédits pour les ressources IANA

Le site IANA fournit un certains nombre de ressources dont un ensemble de registre sur les 
protocoles informatiques.

## Liste des sous-étiquettes de langue

L’un d’entre eux est le registre des subtags 5646, c’est-à-dire la liste officielle des 
sous-étiquettes qui composent les étiquettes de langue utilisées dans les protocoles 
informatiques.

La page est accessible à l’url http://www.iana.org/assignments/language-subtag-registry/language-subtag-registry ou en 
cherchant le lien «Language Subtag Registry» dans la page des protocoles http://www.iana.org/protocols.

Cette page est décryptée par la fonction SPIP idoine sans aucune modification de format ou autre.
