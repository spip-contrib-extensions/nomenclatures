# Crédits pour les infrasubdivisions

Les tables sont fournies via des fichiers CSV (sépateur ";" ou ",").


## US - Liste des comtés américains et territoires outremarins

Le fichier :
* usbcounty_052020.csv

Le fichier est contruit à partir du fichier du Census Bureau [national_county.txt](https://www2.census.gov/geo/docs/reference/codes/files/national_county.txt) que l'on trouve sur la page A partir de la page [American National Standards Institute (ANSI) and Federal Information Processing Series (FIPS) Codes, paragraphe State and State Equivalents](https://www.census.gov/library/reference/code-lists/ansi.html#county) en choissisant United Stated dans la list proposée.

Ce fichier peut être importé sous excel comme un fichier CSV.
