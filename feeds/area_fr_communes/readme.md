# Crédits pour les infrasubdivisions

Les tables sont fournies via des fichiers CSV (sépateur ";" ou ",").

## FR - Liste des communes selon l'INSEE

Le fichier :

* `inseecommune.csv`

Le fichier est construit à partir du source de l'INSEE `v_commune_20xx.csv`, où xx désigne le millésime. On trouve ce fichier millésimé sur la
page [Code Officiel Géographique](https://www.insee.fr/fr/information/2560452). Il faut choisir le millésime le plus récent.

Il faut d'abord supprimer les colonnes inutilisées et ne laisser que TYPECOM, COM, ARR,
NCCENR, CAN et COMPARENT. Ensuite, on ajoute une colonne EPCI pour identifier le lien de parenté
avec un EPCI si besoin. Pour cela, on utilise sous Excel le fichier des communes et celui des
EPCI, l'onglet "Composition_communale" (rechercheV).

Lors de l'import dans un fichier Excel il faut passer chaque colonne une par une pour s'assurer que le format est le
bon et permet de conserver les 0 initiaux dans les références. Pour conserver les accents corrects, le fichier CSV
initial doit avoir une BOM.

Enfin, on exporte le fichier Excel en CSV avec le nom voulu.


## FR - Coordonnées géographiques et code postal des communes

Le fichier :

* `inseecommune-latlon.csv`

Le fichier est construit à partir du source de la poste
[Base officielle des codes postaux](https://datanova.laposte.fr/data-fair/api/v1/datasets/laposte-hexasmal/metadata-attachments/base-officielle-codes-postaux.csv).

Ensuite, seules les colonnes de code INSEE, de coordonnees gps et du code postal sont conservées. On supprime
les doublons du code INSEE car les coordonnées sont toujours identiques, on ajoute le préfixe
"FR-" devant le code COG de la commune et on sépare les coordonnées dans deux colonnes lat et lon.
