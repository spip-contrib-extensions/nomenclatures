# Crédits pour les infrasubdivisions

Les tables sont fournies via des fichiers CSV (sépateur ";" ou ",").


## FR - Liste des arrondissements selon l'INSEE

Le fichier :

* `inseearrond.csv`

Le fichier est construit à partir du source de l'INSEE `v_arrondissement_20xx.csv`, où xx désigne le millésime. On trouve ce fichier millésimé
dans le zip que l'on récupère sur la page [Code Officiel Géographique](https://www.insee.fr/fr/information/2560452). Il faut choisir le millésime le plus récent.

Il suffit de supprimer les colonnes inutilisées et de ne laisser que ARR,DEP et NCCENR. Enfin, on renomme le fichier pour obtenir le nom voulu.

A noter que Paris, comme département possède le numéro 75C et pas 75. On ne le modifie pas dans le fichier car c'est le traitement spécifique qui le fait.
