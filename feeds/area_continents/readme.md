# Crédits pour les ressources GEOIP

GeoIP est une librairie PHP qui permet de connaitre les données géographiques associées à un IP. 
Elle permet en particulier de connaitre le continent désigné par un code à deux lettres.

## Codes des continents

Il existe une fichier json accessible à cette adresse : https://datahub.io/core/continent-codes/r/continent-codes.json 
qui contient le nom anglais et le code du continent.

On le télécharge, on le complète avec le nom français, espagnol, russe, chinois et arabe 
que l'on récupère dans le fichier `m49regions.csv`, source du feed `area_regions` et, on le nomme 
`geoipcontinents.json`.
