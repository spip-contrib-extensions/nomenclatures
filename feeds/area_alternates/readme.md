# Crédits pour les codes alternatifs des territoires

Tables de correspondance entre les codes ISO des subdivions et les autres codes nationaux ou communautaires. Les tables sont fournies via des fichiers CSV (sépateur ";").

## FR - Codes INSEE des régions et départements

Le fichier :

* `iso3166alternates_fr_insee.csv`

Le fichier est construit à partir des sources de l'INSEE `region_2022.csv` et `departement_2022.
csv`. On trouve ces fichiers millésimés sur la page [Code Officiel Géographique](https://www.insee.fr/fr/information/2560452).
Il faut choisir le millésime le plus récent.

On importe ces deux fichiers dans un excel de même que la liste des subdivisions françaises dans un onglet séparés. Pour chaque onglet régions et départements on établit la correspondance avec le code ISO. Les collectivités d'outre-mer sont à la fois dans l'onglet régions et dans l'onglet départements : on les importe deux fois en distinguant le type de code INSEE (région ou département).

## BE - Codes INS

Le fichier :

* `iso3166alternates_be_ins.csv`

Les fichiers sont construits à partir du fichier source Excel [`REFNIS.xlsx`](https://statbel.fgov.be/sites/default/files/Over_Statbel_FR/Nomenclaturen/REFNIS_2019.xls) (date de publication : 01/01/2019) fourni par Statbel.

Pour les régions et provinces, on peut lire les codes INS mais il faut créer à la main la correspondance avec le code ISO.

Page : [REFNIS](https://statbel.fgov.be/fr/propos-de-statbel/methodologie/classifications/geographie)

## DE - Codes ARS

Le fichier :

* `iso3166alternates_de_ars.csv`

Les fichiers sont construits à partir du fichier source Excel [`AuszugGV1QAktuell.xlsx`]
(https://www.destatis.de/DE/Themen/Laender-Regionen/Regionales/Gemeindeverzeichnis/Administrativ/Archiv/GVAuszugQ/AuszugGV1QAktuell.xlsx?__blob=publicationFile) (date de publication :
24/02/2021) fourni par l'ARS.

Pour les lander, on peut lire les codes ARS du land (deux digits) mais il faut créer à la main la correspondance avec le code ISO.

Page : [DESTATIS Regional statistics](https://www.destatis.de/EN/Themes/Countries-Regions/Regional-Statistics/OnlineListMunicipalities/_inhalt.html)


## CH - Codes OFS des cantons

Le fichier :

* `iso3166alternates_ch_ofs.csv`

Le fichier est construit à partir du fichier source Excel `be-b-00.04-agv-01.xlsx` (date de publication : 17/12/2020) fourni par l'OFS et disponible à l'adresse [https://www.bfs.admin.ch/bfsstatic/dam/assets/15264524/master](https://www.bfs.admin.ch/bfsstatic/dam/assets/15264524/master).
Pour les cantons, pour construire le fichier il suffit de créer une colonne en concaténant les informations de l'onglet KT de la
façon suivante : `=CONCATENER("CH-";B2;";code_ktnr;";TEXTE(A2;"00"))`.

Page : [Répertoire officiel des communes de Suisse](https://www.bfs.admin.ch/bfs/fr/home/bases-statistiques/repertoire-officiel-communes-suisse.html)

## NL - Codes CBS

Le fichier :

* `iso3166alternates_nl_cbs.csv`

Pour construire ce fichier XLS il faut récupérer celui des codes CBS des municipalités qui contient
également le lien vers les provinces (nom et code CBS) à partir de la page :
[Gemeentelijke indelingen per jaar](https://www.cbs.nl/nl-nl/onze-diensten/methoden/classificaties/overig/gemeentelijke-indelingen-per-jaar/) en prenant le plus récent.

On extrait uniquement les provinces et leur code CBS dans une feuille à part et manuellement on
ajoute la colonne du code ISO à partir du fichier des subdivisions (il n'y a que 12 provinces).
Enfin, on construit la chaine correspondant à un fichier CSV avec ";" pour le copier dans le
fichier CSV.

# Codes NUTS

Les codes NUTS sont fournis par l'UE à des fins de statistiques. Il reprennent à certains niveaux les subdivisions ISO de chaque pays : il est donc possible de rattacher aux subdivisions de la communauté européenne des codes NUTS.

Le fichier :

* `iso3166alternates_nuts.csv`

Le fichier source est inclus dans le zip des geojson des NUTS de tous les pays européens fournissant ces données. Le zip est à l'adresse [https://gisco-services.ec.europa.eu/distribution/v2/nuts/download/#nuts21](https://gisco-services.ec.europa.eu/distribution/v2/nuts/download/#nuts21) qui présente un tableau de tous les millésimes et de toutes les échelles : ici on prend le geoJSON au 1:60 de 2021.

Dans le zip il existe un fichier CSV `NUTS_AT_2021.txt` que l'on renomme en `.csv` avant de l'utiliser. Il contient la liste des codes NUTS, chaque code étant associé au nom de l'entité statistique. On charge ce CSV dans un fichier excel (onglet NUTS source) et on charge la liste des subdivisions à partir du fichier de nomenclatures `iso3166subdivisions.csv` dans l'onglet subdivisions.

Ensuite, via un RECHERCHEV sur le nom dans l'onglet NUTS source, on essaye de trouver la correspondance avec le code ISO afin de créer les colonnes idoines du fichier final. Malheureusement il existe beaucoup de différence de nommage : il est donc obligatoire de passer un par un les `#NA` pour vérifier si il correspond à une entité non subdivision ISO ou si cela est une erreur de nom pour une subdivision ISO. Dans ce cas, il suffit de mettre le même nom afin de valider la correspondance : en filtrant les `#NA`, on peut finaliser l'onglet NUTS final.


## US - Codes FIPS des états et territoires associés

Le fichier :

* `iso3166alternates_us_fips.csv`

Pour creer ce fichier il faut d'abord se rendre sur la page [Reference Files](https://www.census.gov/geographies/reference-files.2023.List_270653787.html#list-tab-List_270653787) du Census Bureau et identifier le fichier le plus à jour les codes FIPS des entités US répertoriées. Par exemple, fin 2022, le fichier le plus à jour est [all-geocodes-v2021.xlsx](https://www2.census.gov/programs-surveys/popest/geographies/2021/all-geocodes-v2021.xlsx).

On crée dans ce fichier excel un onglet pour les états que l'on remplit avec les états de l'onglet de base que l'on reconnait par leur summary level à 040 (51).
On copie le fichier des subdivisions US et on compare la liste pour identifier la correspondance entre le code ISO et le code FIPS.

A partir de la page [American National Standards Institute (ANSI) and Federal Information Processing Series (FIPS) Codes, paragraphe State and State Equivalents](https://www.census.gov/library/reference/code-lists/ansi.html#state), on complète la liste qui ne contient que les états avec les territoires associés (6) à partir du fichier [state.txt](https://www2.census.gov/geo/docs/reference/state.txt) qui permet aussi de vérifier la liste obtenue. On exporte cette liste pour créer le fichier CSV.
