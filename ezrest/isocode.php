<?php
/**
 * Ce fichier contient l'ensemble des fonctions de service spécifiques à une ou plusieurs collections.
 *
 * @package SPIP\ISOCODE\EZCOLLECTION\SERVICE
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// -----------------------------------------------------------------------
// ----------------------- COLLECTION CONTINENTS -------------------------
// -----------------------------------------------------------------------

/**
 * Récupère la liste des régions du monde de la table spip_m49regions éventuellement filtrées par les critères
 * additionnels positionnés dans la requête.
 *
 * @param array $conditions    Conditions à appliquer au select
 * @param array $filtres       Tableau des critères de filtrage additionnels à appliquer au select.
 * @param array $configuration Configuration de la collection utile pour savoir quelle fonction appeler pour
 *                             construire chaque filtre.
 *
 * @return array Tableau des plugins dont l'index est le préfixe du plugin.
 *               Les champs de type id ou maj ne sont pas renvoyés.
 */
function continents_collectionner(array $conditions, array $filtres, array $configuration) : array {
	// Initialisation de la collection
	$continents = [];

	// Récupérer la liste des régions du monde (pas de filtre possible).
	$from = 'spip_geoipcontinents';
	// -- Tous le champs sauf les labels par langue et la date de mise à jour.
	$description_table = sql_showtable($from, true);
	$champs = array_keys($description_table['field']);
	$select = array_diff($champs, ['maj']);
	// -- Initialisation du where avec les conditions calculées.
	$where = [];
	// -- Si il y a des critères additionnels on complète le where en conséquence en fonction de la configuration.
	if ($conditions) {
		$where = array_merge($where, $conditions);
	}
	// -- Stocker les zones
	$continents['continents'] = sql_allfetsel($select, $from, $where);
	$continents['continents'] = array_column($continents['continents'], null, 'code_num');

	// Déterminer le hash des feeds ayant remplis les zones (uniquement car les continents sont juste un addon)
	include_spip('inc/isocode_feed');
	$continents['hash'] = feed_compiler_hash($continents['continents']);

	return $continents;
}

// -----------------------------------------------------------------------
// -------------------------- COLLECTION ZONES ---------------------------
// -----------------------------------------------------------------------

/**
 * Récupère la liste des régions du monde de la table spip_m49regions éventuellement filtrées par les critères
 * additionnels positionnés dans la requête.
 *
 * @param array $conditions    Conditions à appliquer au select
 * @param array $filtres       Tableau des critères de filtrage additionnels à appliquer au select.
 * @param array $configuration Configuration de la collection utile pour savoir quelle fonction appeler pour
 *                             construire chaque filtre.
 *
 * @return array Tableau des plugins dont l'index est le préfixe du plugin.
 *               Les champs de type id ou maj ne sont pas renvoyés.
 */
function zones_collectionner(array $conditions, array $filtres, array $configuration) : array {
	// Initialisation de la collection
	$zones = [];

	// Récupérer la liste des régions du monde (pas de filtre possible).
	$from = 'spip_m49regions';
	// -- Tous le champs sauf les labels par langue et la date de mise à jour.
	$description_table = sql_showtable($from, true);
	$champs = array_keys($description_table['field']);
	$select = array_diff($champs, ['maj']);
	// -- Initialisation du where avec les conditions calculées.
	$where = [];
	// -- Si il y a des critères additionnels on complète le where en conséquence en fonction de la configuration.
	if ($conditions) {
		$where = array_merge($where, $conditions);
	}
	// -- Stocker les zones
	$zones['zones'] = sql_allfetsel($select, $from, $where);

	// Renvoyer aussi les continents GeoIP
	$from = 'spip_geoipcontinents';
	// -- Tous le champs sauf les labels par langue et la date de mise à jour.
	$description_table = sql_showtable($from);
	$champs = array_keys($description_table['field']);
	$select = array_diff($champs, ['maj']);
	// -- La liste des continents est indexée par le code M49
	$zones['continents'] = sql_allfetsel($select, $from, $where);
	$zones['continents'] = array_column($zones['continents'], null, 'code_num');

	// Déterminer le hash des feeds ayant remplis les zones (uniquement car les continents sont juste un addon)
	include_spip('inc/isocode_feed');
	$zones['hash'] = feed_compiler_hash($zones['zones']);

	return $zones;
}

// -----------------------------------------------------------------------
// -------------------------- COLLECTION PAYS ----------------------------
// -----------------------------------------------------------------------

/**
 * Récupère la liste des pays de la table spip_iso3166countries éventuellement filtrés par les critères
 * additionnels positionnés dans la requête.
 *
 * @param array $conditions    Conditions à appliquer au select
 * @param array $filtres       Tableau des critères de filtrage additionnels à appliquer au select.
 * @param array $configuration Configuration de la collection utile pour savoir quelle fonction appeler pour
 *                             construire chaque filtre.
 *
 * @return array Tableau des plugins dont l'index est le préfixe du plugin.
 *               Les champs de type id ou maj ne sont pas renvoyés.
 */
function pays_collectionner(array $conditions, array $filtres, array $configuration) : array {
	// Initialisation de la collection
	$pays = [];

	// Récupérer la liste des pays (filtrée ou pas).
	// Si la liste est filtrée par continent ou région, on renvoie aussi les informations sur ce continent ou
	// cette région.
	$from = 'spip_iso3166countries';
	// -- Tous le champs sauf les labels par langue et la date de mise à jour.
	$description_table = sql_showtable($from, true);
	$champs = array_keys($description_table['field']);
	$select = array_diff($champs, ['maj']);

	// -- Initialisation du where avec les conditions sur la table des dépots.
	$where = [];
	// -- Si il y a des critères additionnels on complète le where en conséquence en fonction de la configuration.
	if ($conditions) {
		$where = array_merge($where, $conditions);
	}

	$pays['pays'] = sql_allfetsel($select, $from, $where);

	// Renvoyer aussi les continents GeoIP car les pays possèdent un lien vers les continents
	$from = 'spip_geoipcontinents';
	// -- Tous le champs sauf les labels par langue et la date de mise à jour.
	$description_table = sql_showtable($from, true);
	$champs = array_keys($description_table['field']);
	$select = array_diff($champs, ['maj']);

	// La liste des continents est indexée par le code M49
	$pays['continents'] = sql_allfetsel($select, $from, $where);
	$pays['continents'] = array_column($pays['continents'], null, 'code');

	// Déterminer le hash des feeds ayant remplis les pays (uniquement car les continents sont juste un addon)
	include_spip('inc/isocode_feed');
	$pays['hash'] = feed_compiler_hash($pays['pays']);

	return $pays;
}

/**
 * Détermine si la valeur du critère de région d'appartenance du pays est valide.
 * La fonction compare uniquement la structure de la chaine passée qui doit être cohérente avec un code à 3 chiffres.
 *
 * @param string $zone    La valeur du critère région, soit son code ISO 3166-1 numérique (3 chiffres).
 * @param array  &$erreur Bloc d'erreur préparé au cas où la vérification retourne une erreur. Dans ce cas, le bloc
 *                        et complété et renvoyé.
 *
 * @return bool `true` si la valeur est valide, `false` sinon.
 */
function pays_verifier_filtre_zone(string $zone, array &$erreur) : bool {
	$est_valide = true;

	// La liste des régions (code M49)
	$zones = sql_allfetsel('code_num', 'spip_m49regions');
	$zones = array_map('reset', $zones);

	if (!in_array($zone, $zones)) {
		$est_valide = false;
		$erreur['type'] = 'zone_nok';
	}

	return $est_valide;
}

/**
 * Détermine si la valeur du continent d'appartenance du pays est valide.
 * La fonction compare uniquement la structure de la chaine passée qui doit être cohérente avec une code à deux lettres
 * majuscules.
 *
 * @param string $continent La valeur du critère région, soit son code ISO 3166-1 numérique (3 chiffres).
 * @param array  $erreur    Bloc d'erreur préparé au cas où la vérification retourne une erreur. Dans ce cas, le bloc
 *                          et complété et renvoyé.
 *
 * @return bool `true` si la valeur est valide, `false` sinon.
 */
function pays_verifier_filtre_continent(string $continent, array &$erreur) : bool {
	$est_valide = true;

	// La liste des continents (code GeoIP)
	$continents = sql_allfetsel('code', 'spip_geoipcontinents');
	$continents = array_map('reset', $continents);

	if (!in_array($continent, $continents)) {
		$est_valide = false;
		$erreur['type'] = 'continent_nok';
	}

	return $est_valide;
}

// -----------------------------------------------------------------------
// ---------------------- COLLECTION SUBDIVISIONS ------------------------
// -----------------------------------------------------------------------

/**
 * Récupère la liste des subdivisions de la table spip_iso3166subdivisions éventuellement filtrés par les critères
 * additionnels positionnés dans la requête.
 *
 * @param array $conditions    Conditions à appliquer au select
 * @param array $filtres       Tableau des critères de filtrage additionnels à appliquer au select.
 * @param array $configuration Configuration de la collection utile pour savoir quelle fonction appeler pour
 *                             construire chaque filtre.
 *
 * @return array Tableau des subdivisions et par défaut des codes alternatifs et de la liste des pays.
 */
function subdivisions_collectionner(array $conditions, array $filtres, array $configuration) : array {
	// Initialisation de la collection
	$subdivisions = [];

	// Récupérer la liste des subdivisions (filtrée ou pas par pays ou par type de subdivision).
	$from = 'spip_iso3166subdivisions';
	// -- Tous le champs sauf les labels par langue et la date de mise à jour.
	$description_table = sql_showtable($from, true);
	$champs = array_keys($description_table['field']);
	$select = array_diff($champs, ['maj']);

	// -- Initialisation du where: aucune condition par défaut.
	$where = [];
	// -- Si il y a des critères additionnels on complète le where en conséquence.
	if ($conditions) {
		$where = array_merge($where, $conditions);
	}
	// -- Rangement de la liste dans l'index subdivisions
	$subdivisions['subdivisions'] = sql_allfetsel($select, $from, $where);

	// Déterminer le hash des feeds ayant remplis les subdivisions (uniquement car les autres index sont juste un addon).
	// Pour subdivisions on renvoie les deux hash, le global et celui par pays
	include_spip('inc/isocode_feed');
	$subdivisions['hash'] = feed_compiler_hash($subdivisions['subdivisions']);
	$subdivisions['hash_by_country'] = feed_compiler_hash($subdivisions['subdivisions'], 'hash_by_country');

	// La liste est enrichie par défaut:
	// -- des codes alternatifs disponibles dans iso3166alternates
	// -- de la liste des pays concernés par les codes renvoyés
	// Ces données supplémentaires peuvent être exclues en utilisant le filtre 'exclure'
	//
	// -- Ajout des codes alternatifs si non exclus explicitement
	if (empty($filtres['exclure'])
		or (
			!empty($filtres['exclure'])
			and (strpos($filtres['exclure'], 'alternates') === false)
		)
	) {
		// on construit la condition sur la table de liens à partir des codes ISO des subdivisions
		$where = [];
		$codes_subdivision = array_column($subdivisions['subdivisions'], 'code_3166_2');
		$where[] = sql_in('code_iso', $codes_subdivision);

		$codes = sql_allfetsel('*', 'spip_iso3166alternates', $where);
		$subdivisions['codes_alternatifs'] = $codes;
	}

	// -- Ajout de la liste des pays concernés par les subdivisions sauf si exclu
	if (empty($filtres['exclure'])
		or (
			!empty($filtres['exclure'])
			and (strpos($filtres['exclure'], 'pays') === false)
		)
	) {
		// Liste des codes 3166-1 alpha2 et du nom multi
		$pays = [];
		foreach ($subdivisions['subdivisions'] as $_subdivision) {
			if (!in_array($_subdivision['country'], $pays)) {
				$where = ['code_alpha2=' . sql_quote($_subdivision['country'])];
				if ($nom = sql_getfetsel('label', 'spip_iso3166countries', $where)) {
					$pays[$_subdivision['country']] = $nom;
				}
			}
		}
		$subdivisions['pays'] = $pays;

		if (
			!empty($filtres['exclure'])
			and (strpos($filtres['exclure'], 'subdivisions') !== false)
		) {
			// -- On peut exclure aussi la liste des subdivisions pour n'avoir que les pays possédant une subdivisions
			//    Pour cela il ne faut pas avoir exclu la liste des pays car sinon le payload peut être vide.
			//    Cela allège le flux des données.
			unset($subdivisions['subdivisions']);
		}
	}

	return $subdivisions;
}

/**
 * Calcule la condition du filtre pays pour lequel il est possible de passer une liste de codes de pays séparés
 * par une virgule.
 *
 * @param string $valeur Valeur du critère `country`.
 *
 * @return string La condition SQL sur le champ `country`.
 */
function subdivisions_conditionner_pays(string $valeur) : string {
	$condition = '';
	if ($valeur) {
		if (strpos($valeur, ',') === false) {
			// Un seul pays
			$condition = 'country=' . sql_quote($valeur);
		} else {
			// Une liste de pays séparés par des virgules
			$pays = explode(',', $valeur);
			$condition = sql_in('country', $pays);
		}
	}

	return $condition;
}

// -----------------------------------------------------------------------
// -------------------- COLLECTION PROTECTED_AREAS -----------------------
// -----------------------------------------------------------------------

/**
 * Récupère la liste des subdivisions de la table spip_iso3166subdivisions éventuellement filtrés par les critères
 * additionnels positionnés dans la requête.
 *
 * @param array $conditions    Conditions à appliquer au select
 * @param array $filtres       Tableau des critères de filtrage additionnels à appliquer au select.
 * @param array $configuration Configuration de la collection utile pour savoir quelle fonction appeler pour
 *                             construire chaque filtre.
 *
 * @return array Tableau des subdivisions et par défaut des codes alternatifs et de la liste des pays.
 */
function protected_areas_collectionner(array $conditions, array $filtres, array $configuration) : array {
	// Initialisation de la collection
	$protected_areas = [];

	// Récupérer la liste des subdivisions (filtrée ou pas par pays ou par type de subdivision).
	$from = 'spip_wdpas';
	// -- Tous le champs sauf les labels par langue et la date de mise à jour.
	$description_table = sql_showtable($from, true);
	$champs = array_keys($description_table['field']);
	$select = array_diff($champs, ['maj']);

	// -- Initialisation du where: aucune condition par défaut.
	$where = [];
	// -- Si il y a des critères additionnels on complète le where en conséquence.
	if ($conditions) {
		$where = array_merge($where, $conditions);
	}
	// -- Rangement de la liste dans l'index subdivisions
	$protected_areas['protected_areas'] = sql_allfetsel($select, $from, $where);

	// Déterminer le hash des feeds ayant remplis les subdivisions (uniquement car les autres index sont juste un addon).
	// Pour subdivisions on renvoie les deux hash, le global et celui par pays
	include_spip('inc/isocode_feed');
	$protected_areas['hash'] = feed_compiler_hash($protected_areas['protected_areas']);
	$protected_areas['hash_by_country'] = feed_compiler_hash($protected_areas['protected_areas'], 'hash_by_country');

	// La liste est enrichie par défaut:
	// -- des codes alternatifs disponibles dans iso3166alternates
	// -- de la liste des pays concernés par les codes renvoyés
	// Ces données supplémentaires peuvent être exclues en utilisant le filtre 'exclure'
	//
	// -- Ajout des codes alternatifs si non exclus explicitement
	if (empty($filtres['exclure'])
		or (
			!empty($filtres['exclure'])
			and (strpos($filtres['exclure'], 'alternates') === false)
		)
	) {
		// on construit la condition sur la table de liens à partir des codes ISO des subdivisions
		$where = [];
		$codes_pa = array_column($protected_areas['protected_areas'], 'code_wdpa');
		$where[] = sql_in('code_iso', $codes_pa);

		$codes = sql_allfetsel('*', 'spip_iso3166alternates', $where);
		$protected_areas['codes_alternatifs'] = $codes;
	}

	// -- Ajout de la liste des pays concernés par les subdivisions sauf si exclu
	if (empty($filtres['exclure'])
		or (
			!empty($filtres['exclure'])
			and (strpos($filtres['exclure'], 'pays') === false)
		)
	) {
		// Liste des codes 3166-1 alpha2 et du nom multi
		$pays = [];
		foreach ($protected_areas['protected_areas'] as $_subdivision) {
			if (!in_array($_subdivision['country'], $pays)) {
				$where = ['code_alpha2=' . sql_quote($_subdivision['country'])];
				if ($nom = sql_getfetsel('label', 'spip_iso3166countries', $where)) {
					$pays[$_subdivision['country']] = $nom;
				}
			}
		}
		$protected_areas['pays'] = $pays;

		if (
			!empty($filtres['exclure'])
			and (strpos($filtres['exclure'], 'protected_areas') !== false)
		) {
			// -- On peut exclure aussi la liste des subdivisions pour n'avoir que les pays possédant une subdivisions
			//    Pour cela il ne faut pas avoir exclu la liste des pays car sinon le payload peut être vide.
			//    Cela allège le flux des données.
			unset($protected_areas['protected_areas']);
		}
	}

	return $protected_areas;
}

/**
 * Calcule la condition du filtre pays pour lequel il est possible de passer une liste de codes de pays séparés
 * par une virgule.
 *
 * @param string $valeur Valeur du critère `country`.
 *
 * @return string La condition SQL sur le champ `country`.
 */
function protected_areas_conditionner_pays(string $valeur) : string {
	$condition = '';
	if ($valeur) {
		if (strpos($valeur, ',') === false) {
			// Un seul pays
			$condition = 'country=' . sql_quote($valeur);
		} else {
			// Une liste de pays séparés par des virgules
			$pays = explode(',', $valeur);
			$condition = sql_in('country', $pays);
		}
	}

	return $condition;
}

// -----------------------------------------------------------------------
// ------------------- COLLECTION INFRASUBDIVISIONS ----------------------
// -----------------------------------------------------------------------

/**
 * Récupère la liste des subdivisions de la table spip_iso3166subdivisions éventuellement filtrés par les critères
 * additionnels positionnés dans la requête.
 *
 * @param array $conditions    Conditions à appliquer au select
 * @param array $filtres       Tableau des critères de filtrage additionnels à appliquer au select.
 * @param array $configuration Configuration de la collection utile pour savoir quelle fonction appeler pour
 *                             construire chaque filtre.
 *
 * @return array Tableau des subdivisions et par défaut des codes alternatifs et de la liste des pays.
 */
function infrasubdivisions_collectionner(array $conditions, array $filtres, array $configuration) : array {
	// Initialisation de la collection
	$subdivisions = [];

	// Récupérer la liste des subdivisions (filtrée ou pas par pays ou par type de subdivision).
	$from = 'spip_infrasubdivisions';
	// -- Tous le champs sauf les labels par langue et la date de mise à jour.
	$description_table = sql_showtable($from, true);
	$champs = array_keys($description_table['field']);
	$select = array_diff($champs, ['maj']);

	// -- Initialisation du where: aucune condition par défaut.
	$where = [];
	// -- Si il y a des critères additionnels on complète le where en conséquence.
	if ($conditions) {
		$where = array_merge($where, $conditions);
	}
	// -- Rangement de la liste dans l'index subdivisions
	$subdivisions['infrasubdivisions'] = sql_allfetsel($select, $from, $where);

	// Déterminer le hash des feeds ayant remplis les infra-subdivisions (uniquement car les autres index sont juste un addon)
	include_spip('inc/isocode_feed');
	$subdivisions['hash'] = feed_compiler_hash($subdivisions['infrasubdivisions']);

	// La liste est enrichie par défaut:
	// -- des codes alternatifs disponibles dans iso3166alternates
	// -- de la liste des pays concernés par les codes renvoyés
	// Ces données supplémentaires peuvent être exclues en utilisant le filtre 'exclure'
	//
	// -- Ajout des codes alternatifs si non exclus explicitement
	if (empty($filtres['exclure'])
		or (
			!empty($filtres['exclure'])
			and (strpos($filtres['exclure'], 'alternates') === false)
		)
	) {
		// on construit la condition sur la table de liens à partir des codes ISO des subdivisions
		$where = [];
		$codes_subdivision = array_column($subdivisions['infrasubdivisions'], 'code');
		$where[] = sql_in('code_iso', $codes_subdivision);

		$codes = sql_allfetsel('*', 'spip_iso3166alternates', $where);
		$subdivisions['codes_alternatifs'] = $codes;
	}

	// -- Ajout de la liste des pays concernés par les subdivisions sauf si exclu
	if (empty($filtres['exclure'])
		or (
			!empty($filtres['exclure'])
			and (strpos($filtres['exclure'], 'pays') === false)
		)
	) {
		// Liste des codes 3166-1 alpha2 et du nom multi
		$pays = [];
		foreach ($subdivisions['infrasubdivisions'] as $_subdivision) {
			if (!in_array($_subdivision['country'], $pays)) {
				$where = ['code_alpha2=' . sql_quote($_subdivision['country'])];
				if ($nom = sql_getfetsel('label', 'spip_iso3166countries', $where)) {
					$pays[$_subdivision['country']] = $nom;
				}
			}
		}
		$subdivisions['pays'] = $pays;

		if (
			!empty($filtres['exclure'])
			and (strpos($filtres['exclure'], 'infrasubdivisions') !== false)
		) {
			// -- On peut exclure aussi la liste des subdivisions pour n'avoir que les pays possédant une subdivisions
			//    Pour cela il ne faut pas avoir exclu la liste des pays car sinon le payload peut être vide.
			//    Cela allège le flux des données.
			unset($subdivisions['infrasubdivisions']);
		}
	}

	return $subdivisions;
}

/**
 * Calcule la condition du filtre pays pour lequel il est possible de passer une liste de codes de pays séparés
 * par une virgule.
 *
 * @param string $valeur Valeur du critère `country`.
 *
 * @return string La condition SQL sur le champ `country`.
 */
function infrasubdivisions_conditionner_pays(string $valeur) : string {
	$condition = '';
	if ($valeur) {
		if (strpos($valeur, ',') === false) {
			// Un seul pays
			$condition = 'country=' . sql_quote($valeur);
		} else {
			// Une liste de pays séparés par des virgules
			$pays = explode(',', $valeur);
			$condition = sql_in('country', $pays);
		}
	}

	return $condition;
}

// -----------------------------------------------------------------------
// ------------------------ COLLECTION CONTOURS --------------------------
// -----------------------------------------------------------------------

/**
 * Récupère la liste des contours géographiques de la table spip_boundaries.
 * Il est obligatoire de choisir à minima un type de territoire de façon à limiter le transfert d'informations
 * via l'API REST. Un critère facultatif permet de filtrer sur le service ce qui est recommandé.
 *
 * @param array $conditions    Conditions à appliquer au select
 * @param array $filtres       Tableau des critères de filtrage additionnels à appliquer au select.
 * @param array $configuration Configuration de la collection utile pour savoir quelle fonction appeler pour
 *                             construire chaque filtre.
 *
 * @return array Tableau des subdivisions et par défaut des codes alternatifs et de la liste des pays.
 */
function contours_collectionner(array $conditions, array $filtres, array $configuration) : array {
	// Initialisation de la collection
	$contours = [];

	// Récupérer la liste des contours d'un type de territoire donné (filtrée ou pas par service).
	$from = 'spip_geoboundaries';
	// -- Tous le champs sauf les labels par langue et la date de mise à jour.
	$description_table = sql_showtable($from, true);
	$champs = array_keys($description_table['field']);
	$select = array_diff($champs, ['maj']);

	// -- Initialisation du where: aucune condition par défaut.
	$where = [];
	// -- Si il y a des critères additionnels on complète le where en conséquence.
	if ($conditions) {
		$where = array_merge($where, $conditions);
	}
	// -- Rangement de la liste dans l'index contours
	$contours['contours'] = sql_allfetsel($select, $from, $where);
	foreach ($contours['contours'] as $_cle => $_contour) {
		$contours['contours'][$_cle]['geometry'] = json_decode($_contour['geometry'], true);
	}

	// La liste est enrichie par défaut:
	// -- des codes alternatifs disponibles dans iso3166alternates si les contours sont identifiés par
	//    un code alternatif et pas le code standard ISO (3166-1 a2 pour les pays, M49 pour les zones et 3166-2 pour
	//    les subdivisions.
	// Ces données supplémentaires peuvent être exclues en utilisant le filtre 'exclure'
	//
	// -- Ajout des codes alternatifs si non exclus explicitement
	if (empty($filtres['exclure'])
		or (
			!empty($filtres['exclure'])
			and (strpos($filtres['exclure'], 'alternates') === false)
		)
	) {
		// on construit la condition sur la table de liens à partir des codes ISO des subdivisions
		$where = [];
		$types_codes = array_unique(array_column($contours['contours'], 'code_type'));
		$where[] = sql_in('type_alter', $types_codes);

		$codes = sql_allfetsel('*', 'spip_iso3166alternates', $where);
		$contours['codes_alternatifs'] = $codes;
	}

	// On ajoute les crédits.
	$contours['credits'] = [];

	return $contours;
}

/**
 * Calcule la condition du filtre service pour lequel il est possible de passer une liste de services séparés
 * par une virgule.
 *
 * @param string $valeur Valeur du critère `service`.
 *
 * @return string La condition SQL sur le champ `service`.
 */
function contours_conditionner_service($valeur) {
	$condition = '';
	if ($valeur) {
		if (strpos($valeur, ',') === false) {
			// Un seul service
			$condition = 'service=' . sql_quote($valeur);
		} else {
			// Une liste de services séparés par des virgules
			$services = explode(',', $valeur);
			$condition = sql_in('service', $services);
		}
	}

	return $condition;
}

// -----------------------------------------------------------------------
// ----------------- SERVICES NON LIES A UNE COLLECTION ------------------
// -----------------------------------------------------------------------

/**
 * Complète le bloc d'erreur éventuellement retourné par les vérifications concernant le critère `zone` ou `continent`.
 *
 * @param array<string, mixed> $erreur Tableau initialisé avec les éléments de base de l'erreur.
 *
 * @return array<string, mixed> Tableau de l'erreur complété avec le titre et le détail.
 */
function isocode_reponse_expliquer_erreur($erreur) {
	if ($erreur['type'] === 'critere_valeur_format_nok') {
		if ($erreur['element'] === 'zone') {
			$erreur['titre'] = _T('isocode:erreur_400_zone_nok_titre', ['valeur' => $erreur['valeur']]);
			$erreur['detail'] = _T('isocode:erreur_400_zone_nok_message');
		} elseif ($erreur['element'] === 'continent') {
			$erreur['titre'] = _T('isocode:erreur_400_continent_nok_titre', ['valeur' => $erreur['valeur']]);
			$erreur['detail'] = _T('isocode:erreur_400_continent_nok_message');
		}
	}

	return $erreur;
}
