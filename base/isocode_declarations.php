<?php
/**
 * Ce fichier contient les modifications de la base de données requises
 * par le plugin.
 *
 * @package SPIP\ISOCODE\ADMINISTRATION
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Déclaration des nouvelles tables de la base de données propres au plugin.
 *
 * Le plugin déclare des tables ISO-639 issues de 2 bases de données (SIL et Library of Congress
 * uniquement pour les familles de langues) :
 *
 * - `spip_iso639codes`, qui contient les codes ISO-639-3, 2 et 1,
 * - `spip_iso639names`, qui contient les noms de langue,
 * - `spip_iso639macros`, qui contient le mapping des macrolangues,
 * - `spip_iso639retirements`, qui contient les langues retirées de la liste officielle,
 * - `spip_iso639families`, qui contient les familles et groupes de langues ISO-639-5,
 *
 * Le plugin déclare aussi une table `spip_iso15924scripts` qui contient les codets d'écriture à 4 lettres et leur
 * définition en français et en anglais et une table `spip_iana5646subtags` qui contient les codes des sous-étiquettes
 * des étiquettes de langue construites selon la RFC 5646.
 *
 * Le plugin déclare aussi un ensemble de tables liées aux différents découpages géographiques, à savoir:
 * - `spip_geoipcontinents` qui contient les indicatifs GeoIP des continents.
 * - `spip_m49regions` qui contient les indicatifs M49 des zones géographiques englobant les pays.
 * - `spip_iso3166countries` qui contient les indicatifs ISO-3166-1 des pays.
 * - `spip_iso3166subdivisions` qui contient les indicatifs ISO-3166-2 des subdivisions des pays.
 * - `spip_wdpas` qui contient les indicatifs WDPA des zones protégées des pays.
 * - `spip_infrasubdivisions` qui contient les indicatifs nationaux des infra-subdivisions des pays.
 * - `spip_iso3166alternates` qui contient des codes nationaux ou supranationaux alternatifs aux codes IS0 3166 (NUTS, INSEE...).
 *
 * Le plugin déclare aussi la table `spip_geoboundaries` qui contient les contours de territoires.
 *
 * Le plugin déclare enfin une table des devises ISO-4217, `spip_iso4217currencies`.
*
 * @pipeline declarer_tables_principales
 *
 * @param array $tables_principales Tableau global décrivant la structure des tables de la base de données
 *
 * @return array Tableau fourni en entrée et mis à jour avec les nouvelles déclarations
 */
function isocode_declarer_tables_principales(array $tables_principales) : array {
	// ---------------------------------------------------------------
	// Langues
	// ---------------------------------------------------------------
	// Table principale des codes de langue ISO-639 : spip_iso639codes
	$table_codes = [
		'code_639_3'  => "char(3) DEFAULT '' NOT NULL",      // The three-letter 639-3 identifier
		'code_639_2b' => 'char(3)',                          // Equivalent 639-2 identifier of the bibliographic applications code set, if there is one
		'code_639_2t' => 'char(3)',                          // Equivalent 639-2 identifier of the terminology applications code set, if there is one
		'code_639_1'  => 'char(2)',                          // Equivalent 639-1 identifier, if there is one
		'service'     => "varchar(30) DEFAULT '' NOT NULL",  // le nom du feed
		'scope'       => "char(1) DEFAULT '' NOT NULL",      // I(ndividual), M(acrolanguage), S(pecial)
		'type'        => "char(1) DEFAULT '' NOT NULL",      // A(ncient), C(onstructed), E(xtinct), H(istorical), L(iving), S(pecial)
		'ref_name'    => "varchar(150) DEFAULT '' NOT NULL", // Reference language name
		'comment'     => 'varchar(150)',                     // Comment relating to one or more of the columns
		'maj'         => 'timestamp DEFAULT current_timestamp ON UPDATE current_timestamp'
	];

	$table_codes_key = [
		'PRIMARY KEY' => 'code_639_3'
	];

	$tables_principales['spip_iso639codes'] =
		['field' => &$table_codes, 'key' => &$table_codes_key];

	// --------------------------------------------
	// Tables des noms de langue : spip_iso639names
	$table_names = [
		'code_639_3'    => "char(3) DEFAULT '' NOT NULL",     // The three-letter 639-3 identifier
		'service'       => "varchar(30) DEFAULT '' NOT NULL", // le nom du feed
		'print_name'    => "varchar(75) DEFAULT '' NOT NULL", // One of the names associated with this identifier
		'inverted_name' => "varchar(75) DEFAULT '' NOT NULL", // The inverted form of this Print_Name form
		'maj'           => 'timestamp DEFAULT current_timestamp ON UPDATE current_timestamp'
	];

	$table_names_key = [
		'PRIMARY KEY' => 'code_639_3, print_name'
	];

	$tables_principales['spip_iso639names'] =
		['field' => &$table_names, 'key' => &$table_names_key];

	// -------------------------------------------
	// Tables des macrolangues : spip_iso639macros
	$table_macros = [
		'macro_639_3' => "char(3) DEFAULT '' NOT NULL",       // The identifier for a macrolanguage
		'code_639_3'  => "char(3) DEFAULT '' NOT NULL",       // The identifier for an individual language that is a member of the macrolanguage
		'service'     => "varchar(30) DEFAULT '' NOT NULL",   // le nom du feed
		'status'      => "char(1) DEFAULT '' NOT NULL",       // A (active) or R (retired) indicating the status of the individual code element
		'maj'         => 'timestamp DEFAULT current_timestamp ON UPDATE current_timestamp'
	];

	$table_macros_key = [
		'PRIMARY KEY' => 'macro_639_3, code_639_3'
	];

	$tables_principales['spip_iso639macros'] =
		['field' => &$table_macros, 'key' => &$table_macros_key];

	// ------------------------------------------------------
	// Tables des langues supprimées : spip_iso639retirements
	$table_rets = [
		'code_639_3'     => "char(3) DEFAULT '' NOT NULL",      // The three-letter 639-3 identifier
		'service'        => "varchar(30) DEFAULT '' NOT NULL",  // le nom du feed
		'ref_name'       => "varchar(150) DEFAULT '' NOT NULL", // Reference language name
		'ret_reason'     => "char(1) DEFAULT '' NOT NULL",      // code for retirement: C (change), D (duplicate), N (non-existent), S (split), M (merge)
		'change_to'      => 'char(3)',                          // in the cases of C, D, and M, the identifier to which all instances of this Id should be changed
		'ret_remedy'     => 'varchar(300)',                     // The instructions for updating an instance of the retired (split) identifier
		'effective_date' => "datetime DEFAULT '0000-00-00 00:00:00' NOT NULL", // The date the retirement became effective
		'maj'            => 'timestamp DEFAULT current_timestamp ON UPDATE current_timestamp'
	];

	$table_rets_key = [
		'PRIMARY KEY' => 'code_639_3'
	];

	$tables_principales['spip_iso639retirements'] =
		['field' => &$table_rets, 'key' => &$table_rets_key];

	// ---------------------------------------------------------------------------
	// Tables des familles et groupes de langues (ISO-639-5) : spip_iso639families
	$table_families = [
		'code_639_5' => "char(3) DEFAULT '' NOT NULL",      // The three-letter 639-5 identifier
		'service'    => "varchar(30) DEFAULT '' NOT NULL",  // le nom du feed
		'uri'        => "varchar(150) DEFAULT '' NOT NULL", // Description page
		'label'      => "text DEFAULT '' NOT NULL",         // Multiple langages label for the family
		'scope'      => "char(1) DEFAULT '' NOT NULL",      // C(ollective) always
		'code_set'   => "varchar(32) DEFAULT '' NOT NULL",  // Any combinaison of 639-5 and 639-2 separed by comma
		'hierarchy'  => "varchar(32) DEFAULT '' NOT NULL",  // List of 639-5 identifiers separated by comma
		'parent'     => "char(3) DEFAULT '' NOT NULL",      // The parent three-letter 639-5 identifier
		'maj'        => 'timestamp DEFAULT current_timestamp ON UPDATE current_timestamp'
	];

	$table_families_key = [
		'PRIMARY KEY' => 'code_639_5'
	];

	$tables_principales['spip_iso639families'] =
		['field' => &$table_families, 'key' => &$table_families_key];

	// -------------------------------------------------------------------
	// Table des indicatifs d'écritures (ISO 15924) : spip_iso15924scripts
	$table_scripts = [
		'code_15924' => "char(4) DEFAULT '' NOT NULL",                     // The four-letter identifier
		'service'    => "varchar(30) DEFAULT '' NOT NULL",                 // le nom du feed
		'label'      => "text DEFAULT '' NOT NULL",                        // Multiple langages label
		'code_num'   => "char(3) DEFAULT '' NOT NULL",                     // Numeric identifier
		'alias_en'   => "varchar(32) DEFAULT '' NOT NULL",                 // Unicode alias showing how ISO 15924 code relate to script names defined in Unicode.
		'date_ref'   => "datetime DEFAULT '0000-00-00 00:00:00' NOT NULL", // The reference date to follow changes
		'maj'        => 'timestamp DEFAULT current_timestamp ON UPDATE current_timestamp'
	];

	$table_scripts_key = [
		'PRIMARY KEY' => 'code_15924'
	];

	$tables_principales['spip_iso15924scripts'] =
		['field' => &$table_scripts, 'key' => &$table_scripts_key];

	// -----------------------------------------------------------------------------------------
	// Table reproduisant le registre IANA des sous-étiquettes de langues : spip_iana5646subtags
	$table_subtags = [
		'type'           => "varchar(16) DEFAULT '' NOT NULL", // Subtag type as language, variant, extlang, region, script...
		'subtag'         => "varchar(32) DEFAULT '' NOT NULL", // Subtag value
		'service'        => "varchar(30) DEFAULT '' NOT NULL", // le nom du feed
		'description'    => "text DEFAULT '' NOT NULL",        // Descriptions of subtags separated by comma
		'date_ref'       => "datetime DEFAULT '0000-00-00 00:00:00' NOT NULL", // Subtag creation date
		'no_script'      => "char(4) DEFAULT '' NOT NULL",     // The four letter script identifier not to be used for the subtag
		'scope'          => "varchar(32) DEFAULT '' NOT NULL", // Scope indication : collection, macrolanguage...
		'macro_language' => "char(3) DEFAULT '' NOT NULL",     // Macrolanguage to which subtag is refering to
		'deprecated'     => "datetime DEFAULT '0000-00-00 00:00:00' NOT NULL", // Deprecated date if any
		'preferred_tag'  => "char(3) DEFAULT '' NOT NULL",     // Preferred tag to be used instead the current subtag
		'prefix'         => "char(3) DEFAULT '' NOT NULL",     // Prefix to be used thos the subtag except is a preferred tag exists
		'comments'       => "text DEFAULT '' NOT NULL",        // Comments on subtag
		'maj'            => 'timestamp DEFAULT current_timestamp ON UPDATE current_timestamp'
	];

	$table_subtags_key = [
		'PRIMARY KEY' => 'type, subtag'
	];

	$tables_principales['spip_iana5646subtags'] =
		['field' => &$table_subtags, 'key' => &$table_subtags_key];

	// -------------------------------------------------------------------------------------
	// Identifiants & informations sur les zones géographiques du monde aux subdivisions
	// -------------------------------------------------------------------------------------
	// Table des indicatifs des continents GeoIP : spip_geoipcontinents
	$table_continents = [
		'code'     => "char(2) DEFAULT '' NOT NULL",       // The two-letter identifier
		'code_num' => "varchar(3) DEFAULT '' NOT NULL",    // The three-digit M49 identifier
		'service'  => "varchar(30) DEFAULT '' NOT NULL",   // le nom du feed
		'label'    => "text DEFAULT '' NOT NULL",          // Multiple langages label
		'maj'      => 'timestamp DEFAULT current_timestamp ON UPDATE current_timestamp'
	];

	$table_continents_key = [
		'PRIMARY KEY' => 'code'
	];

	$tables_principales['spip_geoipcontinents'] =
		['field' => &$table_continents, 'key' => &$table_continents_key];

	// -------------------------------------------------------------------------------------
	// Table des indicatifs des régions du monde (arborescence) : spip_m49regions
	$table_regions = [
		'code_num' => "char(3) DEFAULT '' NOT NULL",       // The three-letter numeric identifier
		'service'  => "varchar(30) DEFAULT '' NOT NULL",   // le nom du feed
		'category' => "varchar(64) DEFAULT '' NOT NULL",   // Category like region, continent or world
		'parent'   => "char(3) DEFAULT '' NOT NULL",       // The three-letter numeric identifier of parent
		'depth'    => 'int DEFAULT 0 NOT NULL',            // Tree depth starting from 0 (top) to n
		'label'    => "text DEFAULT '' NOT NULL",          // Multiple langages label
		'maj'      => 'timestamp DEFAULT current_timestamp ON UPDATE current_timestamp'
	];

	$table_regions_key = [
		'PRIMARY KEY' => 'code_num'
	];

	$tables_principales['spip_m49regions'] =
		['field' => &$table_regions, 'key' => &$table_regions_key];

	// -------------------------------------------------------------------------------------
	// Table des indicatifs des pays ISO-3166-1 et autres informations : spip_iso3166countries
	$table_countries = [
		'code_alpha2'     => "char(2) DEFAULT '' NOT NULL",       // The two-letter identifier
		'code_alpha3'     => "char(3) DEFAULT '' NOT NULL",       // The three-letter identifier
		'code_num'        => "char(3) DEFAULT '' NOT NULL",       // Numeric identifier
		'service'         => "varchar(30) DEFAULT '' NOT NULL",   // le nom du feed
		'category'        => "varchar(64) DEFAULT '' NOT NULL",   // Category (area, country) only country used today
		'label'           => "text DEFAULT '' NOT NULL",          // Multiple langages label
		'capital'         => "varchar(255) DEFAULT '' NOT NULL",  // Capital name
		'area'            => 'int DEFAULT 0 NOT NULL',            // Area in squared km
		'population'      => 'int DEFAULT 0 NOT NULL',            // Inhabitants count
		'code_continent'  => "char(2) DEFAULT '' NOT NULL",       // Continent code alpha2
		'code_num_region' => "char(3) DEFAULT '' NOT NULL",       // Parent region numeric code (ISO 3166)
		'tld'             => "char(3) DEFAULT '' NOT NULL",       // Tld - Top-Level Domain
		'code_4217_3'     => "char(3) DEFAULT '' NOT NULL",       // Currency code ISO-4217
		'currency_en'     => "varchar(255) DEFAULT '' NOT NULL",  // Currency English name
		'phone_id'        => "varchar(16) DEFAULT '' NOT NULL",   // Phone id
		'maj'             => 'timestamp DEFAULT current_timestamp ON UPDATE current_timestamp'
	];

	$table_countries_key = [
		'PRIMARY KEY'     => 'code_alpha2',
		'KEY code_alpha3' => 'code_alpha3',
		'KEY code_num'    => 'code_num',
	];

	$tables_principales['spip_iso3166countries'] =
		['field' => &$table_countries, 'key' => &$table_countries_key];

	// -------------------------------------------------------------------------------------
	// Table des subdivisions géographiques des pays (arborescence) suivant l'ISO 3166-2 : spip_iso3166subdivisions
	$table_subdivisions = [
		'code_3166_2' => "varchar(16) DEFAULT '' NOT NULL",   // ISO 3166-2 Subdivision identifier XX-YYY or specific id (WDPA for nature parks)
		'service'     => "varchar(30) DEFAULT '' NOT NULL",   // le nom du feed
		'country'     => "char(2) DEFAULT '' NOT NULL",       // The two-letter identifier (ISO 3166 alpha2)
		'type'        => "varchar(64) DEFAULT '' NOT NULL",   // Specific type by country in english (department, land...)
		'parent'      => "varchar(6) DEFAULT '' NOT NULL",    // The ISO 3166-2 identifier of parent
		'depth'       => 'int DEFAULT 0 NOT NULL',            // Tree depth starting from 0 (top) to n
		'label'       => "text DEFAULT '' NOT NULL",          // Multilangage name
		'maj'         => 'timestamp DEFAULT current_timestamp ON UPDATE current_timestamp'
	];

	$table_subdivisions_key = [
		'PRIMARY KEY' => 'code_3166_2',
	];

	$tables_principales['spip_iso3166subdivisions'] =
		['field' => &$table_subdivisions, 'key' => &$table_subdivisions_key];

	// -------------------------------------------------------------------------------------
	// Table des zones protégées des pays suivant le WDPA : spip_wdpas
	$table_protected_areas = [
		'code_wdpa'     => "varchar(16) DEFAULT '' NOT NULL",   // ISO 3166-2 Subdivision identifier XX-YYY or specific id (WDPA for nature parks)
		'service'       => "varchar(30) DEFAULT '' NOT NULL",   // le nom du feed
		'country'       => "char(2) DEFAULT '' NOT NULL",       // The two-letter identifier (ISO 3166 alpha2)
		'type'          => "varchar(64) DEFAULT '' NOT NULL",   // Specific type by country in english (department, land...)
		'parent'        => "varchar(6) DEFAULT '' NOT NULL",    // The ISO 3166-2 identifier of parent
		'label'         => "text DEFAULT '' NOT NULL",          // Multilangage name
		'area'          => 'int DEFAULT 0 NOT NULL',            // Area in squared km
		'creation_date' => "datetime DEFAULT '0000-00-00 00:00:00' NOT NULL",
		'maj'           => 'timestamp DEFAULT current_timestamp ON UPDATE current_timestamp'
	];

	$table_protected_areas_key = [
		'PRIMARY KEY' => 'code_wdpa',
	];

	$tables_principales['spip_wdpas'] =
		['field' => &$table_protected_areas, 'key' => &$table_protected_areas_key];

	// -------------------------------------------------------------------------------------
	// Table des infra subdivisions géographiques des pays (arborescence) avec un code spécifiques : spip_infrasubdivisions
	$table_infra_subdivisions = [
		'service'     => "varchar(30) DEFAULT '' NOT NULL",   // le nom du feed
		'code'        => "varchar(20) DEFAULT '' NOT NULL",   // Specific identifier based on alpha2 country and specific code XX-YYY
		'country'     => "char(2) DEFAULT '' NOT NULL",       // The two-letter identifier (ISO 3166 alpha2)
		'type'        => "varchar(64) DEFAULT '' NOT NULL",   // Specific type by country in english (department, land...)
		'parent'      => "varchar(20) DEFAULT '' NOT NULL",   // The identifier of parent
		'depth'       => 'int DEFAULT 0 NOT NULL',            // Tree depth starting from 0 (top) to n
		'parent_alt1' => "varchar(20) DEFAULT '' NOT NULL",   // The identifier of parent as 1st alternative
		'parent_alt2' => "varchar(20) DEFAULT '' NOT NULL",   // The identifier of parent as 2nd alternative
		'label'       => "text DEFAULT '' NOT NULL",          // Multilangage name
		'postcode'    => "varchar(10) DEFAULT '' NOT NULL",   // Postal code if any
		'lat'         => 'double NULL NULL',                  // Coordinates : latitude
		'lon'         => 'double NULL NULL',                  // Coordinates : longitude
		'maj'         => 'timestamp DEFAULT current_timestamp ON UPDATE current_timestamp'
	];

	$table_infra_subdivisions_key = [
		'PRIMARY KEY' => 'service,code',
		'KEY type'    => 'type',
		'KEY country' => 'country',
	];

	$tables_principales['spip_infrasubdivisions'] =
		['field' => &$table_infra_subdivisions, 'key' => &$table_infra_subdivisions_key];

	// -------------------------------------------------------------------------------------
	// Table des codes alternatifs à l'ISO 3166-2 (nationaux ou supra-nationaux) : spip_iso3166alternates
	// C'est en quelque sorte une table de liens qui fonctionne avec les codes et pas un id.
	$table_alternates = [
		'code_iso'   => "varchar(20) DEFAULT '' NOT NULL",
		'type_alter' => "varchar(16) DEFAULT '' NOT NULL",
		'code_alter' => "varchar(20) DEFAULT '' NOT NULL",
		'service'    => "varchar(30) DEFAULT '' NOT NULL",   // le nom du feed
	];

	$table_alternates_key = [
		'PRIMARY KEY' => 'code_iso,type_alter,code_alter',
	];

	$tables_principales['spip_iso3166alternates'] =
		['field' => &$table_alternates, 'key' => &$table_alternates_key];

	// ------------------------------------------------------------------
	// Table des indicatifs des devises ISO-4217 : spip_iso4217currencies
	$table_currencies = [
		'code_4217_3' => "char(3) DEFAULT '' NOT NULL",       // The three-letter identifier
		'code_num'    => "char(3) DEFAULT '' NOT NULL",       // Numeric identifier
		'service'     => "varchar(30) DEFAULT '' NOT NULL",   // le nom du feed
		'label'       => "text DEFAULT '' NOT NULL",          // Multiple langages label
		'symbol'      => "char(8) DEFAULT '' NOT NULL",       // Currency symbol
		'minor_units' => 'int DEFAULT 0 NOT NULL',            // Minor units
		'maj'         => 'timestamp DEFAULT current_timestamp ON UPDATE current_timestamp'
	];

	$table_currencies_key = [
		'PRIMARY KEY' => 'code_4217_3'
	];

	$tables_principales['spip_iso4217currencies'] =
		['field' => &$table_currencies, 'key' => &$table_currencies_key];

	// -------------------------------------------------------------------------------------
	// Géométrie des zones géographiques du monde aux subdivisions
	// -------------------------------------------------------------------------------------
	// Table des geometries des territoires : spip_geoboundaries
	$table_geometries = [
		'service'   => "varchar(30) DEFAULT '' NOT NULL", // le nom du service geo
		'code'      => "varchar(10) DEFAULT '' NOT NULL", // L'identifiant de l'objet
		'code_type' => "varchar(30) DEFAULT '' NOT NULL", // Le type d'identifiant
		'type'      => "varchar(30) DEFAULT '' NOT NULL", // type vu de l'API REST
		'country'   => "varchar(2) DEFAULT '' NOT NULL",  // Code ISO 3166-1 alpha2 (utile si type = subdivision)
		'lat'       => 'double NULL NULL',
		'lon'       => 'double NULL NULL',
		'geometry'  => 'longtext NOT NULL DEFAULT ""',    // La géométrie de l'objet (json)
		'format'    => "varchar(10) DEFAULT '' NOT NULL", // format de la géométrie (geoJSON, topoJSON, etc)
		'maj'       => 'timestamp DEFAULT current_timestamp ON UPDATE current_timestamp'
	];

	$table_geometries_key = [
		'PRIMARY KEY' => 'service,code',
		'KEY type'    => 'type',
		'KEY country' => 'country',
	];

	$tables_principales['spip_geoboundaries'] =
		['field' => &$table_geometries, 'key' => &$table_geometries_key];

	return $tables_principales;
}

/**
 * Déclaration des informations tierces (alias, traitements, jointures, etc)
 * sur les tables de la base de données modifiées ou ajoutées par le plugin.
 *
 * Le plugin se contente de déclarer les alias des tables qu'il ajoute.
 *
 * @pipeline declarer_tables_interfaces
 *
 * @param array $interfaces Tableau global des informations tierces sur les tables de la base de données
 *
 * @return array Tableau fourni en entrée et mis à jour avec les nouvelles informations
 */
function isocode_declarer_tables_interfaces(array $interfaces) : array {
	// Les tables
	// - catégorie language
	$interfaces['table_des_tables']['iso639codes'] = 'iso639codes';
	$interfaces['table_des_tables']['iso639names'] = 'iso639names';
	$interfaces['table_des_tables']['iso639macros'] = 'iso639macros';
	$interfaces['table_des_tables']['iso639retirements'] = 'iso639retirements';
	$interfaces['table_des_tables']['iso639families'] = 'iso639families';
	$interfaces['table_des_tables']['iso15924scripts'] = 'iso15924scripts';
	$interfaces['table_des_tables']['iana5646subtags'] = 'iana5646subtags';
	// - catégorie misc
	$interfaces['table_des_tables']['iso4217currencies'] = 'iso4217currencies';
	// - catégorie area
	$interfaces['table_des_tables']['geoipcontinents'] = 'geoipcontinents';
	$interfaces['table_des_tables']['m49regions'] = 'm49regions';
	$interfaces['table_des_tables']['iso3166countries'] = 'iso3166countries';
	$interfaces['table_des_tables']['iso3166subdivisions'] = 'iso3166subdivisions';
	$interfaces['table_des_tables']['infrasubdivisions'] = 'infrasubdivisions';
	$interfaces['table_des_tables']['wdpas'] = 'wdpas';
	$interfaces['table_des_tables']['iso3166alternates'] = 'iso3166alternates';
	// - catégorie map
	$interfaces['table_des_tables']['geoboundaries'] = 'geoboundaries';

	return $interfaces;
}
