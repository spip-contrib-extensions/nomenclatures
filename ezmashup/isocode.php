<?php
/**
 * Ce fichier contient les fonctions de service de Mashup Factory personnalisées par le plugin Nomenclatures
 * et ne dépendant que du plugin.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// ------------------------------------------------------------
// ------------------------- FEEDS ----------------------------
// ------------------------------------------------------------

/**
 * Renvoie la liste des catégories et leur description.
 *
 * Le plugin Mashup Factory fournit une liste limitée à la catégorie d'identifiant `default`.
 *
 * @return array Liste des catégories et de leur description au format [id] = tableau de description avec le nom (label),
 *               la description et l'icone.
 */
function isocode_feed_categorie_lister() : array {
	// Initialisation des catégories par défaut
	return [
		'territory' => [
			'name'        => '<:isocode:label_feed_category_territory:>',
			'description' => '<:isocode:description_feed_category_territory:>',
			'icon'        => 'territory-24.svg'
		],
		'map' => [
			'name'        => '<:isocode:label_feed_category_map:>',
			'description' => '<:isocode:description_feed_category_map:>',
			'icon'        => 'map-24.svg'
		],
		'map_pa' => [
			'name'        => '<:isocode:label_feed_category_map_pa:>',
			'description' => '<:isocode:description_feed_category_map_pa:>',
			'icon'        => 'map-24.svg'
		],
		'language' => [
			'name'        => '<:isocode:label_feed_category_language:>',
			'description' => '<:isocode:description_feed_category_language:>',
			'icon'        => 'langage-24.svg'
		],
		'default' => [
			'name'        => '<:isocode:label_feed_category_default:>',
			'description' => '<:isocode:description_feed_category_default:>',
			'icon'        => 'defaut-24.svg'
		]
	];
}

/**
 * Renvoie l'identifiant de la catégorie par défaut d'un plugin utilisateur.
 *
 * Le plugin Mashup Factory choisit la première catégorie de la liste.
 *
 * @return string Identifiant de la catégorie par défaut.
 */
function isocode_feed_categorie_initialiser_defaut() : string {
	return 'default';
}
