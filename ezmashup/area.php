<?php
/**
 * Ce fichier contient l'ensemble des constantes et fonctions implémentant le type de service INFRASUBDIVISION.
 * Les services de INFRASUBDIVISION sont des mécanisme internes à Nomenclatures pour
 * charger les codes nationaux des infra subdivisions fournis par des services divers.
 *
 * @package SPIP\ISOCODE\SERVICES\INFRASUBDIVISION
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// -----------------------------------------------------------------------------------
// ---------- API du type de service INFRASUBDIVISION - Actions principales ----------
// -----------------------------------------------------------------------------------

/**
 * Complète un item extrait de la source via le service `territory_fr_arrondissements` (avant son évolution en enregistrement
 * de la table `infrasubdivisions`).
 *
 * @param array $item Item extrait de la source.
 * @param array $feed Configuration du feed.
 *
 * @return array Item complété.
 */
function area_fr_arrondissements_item_completer(array $item, array $feed) : array {
	// Cette fonction permet d'apporter des corrections au champs déjà compilés : attention on a encore les index source !!!
	// - modifier l'identifiant de l'arrondissement
	$item['ARR'] = 'FR-' . $item['ARR'];
	// - Modifier l'identifiant du parent qui est une subdivision
	//   Pour Paris, 75 on utilise le nouveau code ISO, '75C'
	if ((int) $item['DEP'] === 75) {
		$item['DEP'] = '75C';
	}
	$item['DEP'] = 'FR-' . $item['DEP'];

	return $item;
}

/**
 * Complète un item extrait de la source via le service `territory_fr_epci` (avant son évolution en enregistrement
 * de la table `infrasubdivisions`).
 *
 * @param array $item Item extrait de la source.
 * @param array $feed Configuration du feed.
 *
 * @return array Item complété.
 */
function area_fr_epci_item_completer(array $item, array $feed) : array {
	// Cette fonction permet d'apporter des corrections au champs déjà compilés : attention on a encore les index source !!!
	// - modifier l'identifiant de l'EPCI
	$item['EPCI'] = 'FR-' . $item['EPCI'];
	// - Modifier l'identifiant du parent qui est une subdivision
	if ((int) $item['DEP'] === 75) {
		$item['DEP'] = '75C';
	}
	$item['DEP'] = 'FR-' . $item['DEP'];
	// - affiner l'identifiant du type d'EPCI
	switch ($item['NATURE_EPCI']) {
		case 'CC':
			$item['NATURE_EPCI'] = 'municipality_community';
			break;
		case 'CA':
			$item['NATURE_EPCI'] = 'agglomeration_community';
			break;
		case 'CU':
			$item['NATURE_EPCI'] = 'urban_community';
			break;
		case 'ME':
		case 'METRO':
			$item['NATURE_EPCI'] = 'metropolis';
			break;
		default:
			$item['NATURE_EPCI'] = 'epci';
	}

	return $item;
}

/**
 * Complète un item extrait de la source via le service `area_fr_communes` (avant son évolution en enregistrement
 * de la table `infrasubdivisions`).
 *
 * @param array $item Item extrait de la source.
 * @param array $feed Configuration du feed.
 *
 * @return array Item complété.
 */
function area_fr_communes_item_completer(array $item, array $feed) : array {
	// Cette fonction permet d'apporter des corrections au champs déjà compilés : attention on a encore les index source !!!
	// - modifier l'identifiant de l'arrondissement
	$item['COM'] = 'FR-' . $item['COM'];
	// - choisir le "bon" parent et modifier son identifiant
	if ($item['COMPARENT']) {
		$item['ARR'] = 'FR-' . $item['COMPARENT'];
	} elseif ($item['ARR']) {
		$item['ARR'] = 'FR-' . $item['ARR'];
	} else {
		$item['ARR'] = 'FR-' . $item['DEP'];
	}
	// - modifier l'identifiant de l'EPCI si besoin
	if ($item['EPCI']) {
		$item['EPCI'] = 'FR-' . $item['EPCI'];
	}
	// - affiner l'identifiant du type de commune
	switch ($item['TYPECOM']) {
		case 'COMD':
			$item['TYPECOM'] = 'delegated_commune';
			// Eviter les doublons d'identifiant pour certaines communes déléguées
			$item['COM'] = $item['COM'] . '-D';
			break;
		case 'COMA':
			$item['TYPECOM'] = 'associated_commune';
			break;
		case 'ARM':
			$item['TYPECOM'] = 'municipal_arrondissement';
			break;
		default:
			$item['TYPECOM'] = 'commune';
	}

	return $item;
}

/**
 * Complète les enregistrements de base du feed `area_fr_communes`.
 * La fonction ajoute la latitude et la longitude du point central de chaque commune.
 *
 * @param array $enregistrements Liste des enregistrements déjà remplis à partir de la source.
 * @param array $feed            Configuration du feed
 *
 * @throws Exception
 *
 * @return array Liste des enregistrements complétés.
 */
function area_fr_communes_record_list_completer(array $enregistrements, array $feed) : array {
	// Lecture du fichier CSV de la latitude et longitude.
	include_spip('inc/ezmashup_dataset_source');
	$id_source = 'latlonpost_commune';
	$source = dataset_source_extraire($id_source, $feed, $erreur);
	// On indexe le tableau par le code ISO
	$source = array_column($source, null, 'iso');

	// On complète maintenant le tableau des enregistrements avec les informations géographiques
	foreach ($enregistrements as $_cle => $_enregistrement) {
		if (isset($source[$_enregistrement['code']])) {
			foreach ($feed['mapping']['addon_fields'][$id_source] as $_champ_target => $_champ_source) {
				$enregistrements[$_cle][$_champ_target] = table_valeur($source[$_enregistrement['code']], $_champ_source);
			}
		}
	}

	return $enregistrements;
}

/**
 * Complète les enregistrements de base du feed `area_countries`.
 * La fonction ajoute :
 * - des informations sur le pays
 * - le lien avec la région d'appartenance
 * - des traductions si nécessaire.
 *
 * @param array $enregistrements Liste des enregistrements déjà remplis à partir de la source.
 * @param array $feed            Configuration du feed
 *
 * @throws Exception
 *
 * @return array Liste des enregistrements complétés.
 */
function area_countries_record_list_completer(array $enregistrements, array $feed) : array {
	// Lecture du fichier CSV des informations Geonames.
	include_spip('inc/ezmashup_dataset_source');
	$id_source_inf = 'geonames_infos';
	$source_inf = dataset_source_extraire($id_source_inf, $feed, $erreur);
	// On indexe le tableau par le code ISO
	$source_inf = array_column($source_inf, null, 'ISO');

	// Lecture du fichier CSV de l'appartenance pays/région
	$id_source_reg = 'm49region_links';
	$source_reg = dataset_source_extraire($id_source_reg, $feed, $erreur);
	// On indexe le tableau par le code ISO
	$source_reg = array_column($source_reg, null, 'code_num');

	// Lecture des fichiers de traductions UNTERM et compilation d'un tableau de traductions par pays concerné.
	$traductions = [];
	foreach (['trad_unterm_iso', 'trad_unterm_add', 'trad_unterm_dif'] as $_id_source) {
		$source = dataset_source_extraire($_id_source, $feed, $erreur);
		$source = array_column($source, null, 'iso2');
		$traductions = array_merge($traductions, $source);
	}

	// On complète maintenant le tableau des enregistrements avec les informations glanées dans le fichier geonames
	// et dans celui du M49.
	foreach ($enregistrements as $_cle => $_enregistrement) {
		// La région M49
		$code = $_enregistrement['code_num'];
		if (isset($source_reg[$code])) {
			foreach ($feed['mapping']['addon_fields'][$id_source_reg] as $_champ_target => $_champ_source) {
				$enregistrements[$_cle][$_champ_target] = table_valeur($source_reg[$code], $_champ_source);
			}
		}

		// Les informations Geonames
		$code = $_enregistrement['code_alpha2'];
		if (isset($source_inf[$code])) {
			foreach ($feed['mapping']['addon_fields'][$id_source_inf] as $_champ_target => $_champ_source) {
				$enregistrements[$_cle][$_champ_target] = table_valeur($source_inf[$code], $_champ_source);
			}
		}

		// Les traductions UNTERM
		if (isset($traductions[$code])) {
			include_spip('ezmashup/ezmashup');
			// Des traductions sont disponibles pour mise à jour
			$traductions_pays = $traductions[$code];
			unset($traductions_pays['iso2']);
			$enregistrements[$_cle]['label'] = ezmashup_record_completer_traduction(
				$enregistrements[$_cle]['label'],
				$traductions_pays
			);
		}
	}

	return $enregistrements;
}

/**
 * Complète un enregistrement du feed `area_subdivisions`.
 *
 * @param array $enregistrement Enregistrement du dataset cible déjà rempli
 * @param array $feed           Configuration du feed
 *
 * @return array Enregistrement complété.
 */
function area_subdivisions_record_completer(array $enregistrement, array $feed) : array {
	// S'assurer que la catégorie est en minuscule
	$enregistrement['type'] = strtolower($enregistrement['type']);

	return $enregistrement;
}

/**
 * Fusionne un enregistrement avec un autre du feed `area_subdivisions` : cela est utile uniquement pour créer un multi du label.
 *
 * @param array $enregistrement_en_cours Enregistrement du dataset cible en cours de constitution
 * @param array $enregistrement_en_liste Enregistrement du dataset cible déjà dans la liste
 * @param array $feed                    Configuration du feed
 *
 * @return array La liste des enregistrements mise à jour avec la fusion sur l'enregistrement concerné.
 */
function area_subdivisions_record_fusionner(array $enregistrement_en_cours, array $enregistrement_en_liste, array $feed) : array {
	// Cette fonction est appelée alors qu'il existe au moins un premier enregistrement ayant la clé primaire concernée.
	// De fait, il suffit de compléter le champ label avec la traduction dans la langue de l'enregistrement courant.
	$enregistrement = $enregistrement_en_liste;
	$enregistrement['label'] = '<multi>'
		. str_replace(['<multi>', '</multi>'], '', $enregistrement_en_liste['label'])
		. "[{$enregistrement_en_cours['language']}]"
		. $enregistrement_en_cours['label']
		. '</multi>';

	return $enregistrement;
}

/**
 * Complète les enregistrements du `area_subdivisions`.
 *
 * @param array $enregistrements Liste des enregistrements déjà remplis à partir de la source.
 * @param array $feed            Configuration du feed
 *
 * @throws Exception
 *
 * @return array Liste des enregistrements complétés.
 */
function area_subdivisions_record_list_completer(array $enregistrements, array $feed) : array {
	// Lecture des fichiers de traductions au format iso3166subdivisions_trad_<langue>.csv qui contiennent
	// 2 colonnes : Code ISO 3166-2 et le nom traduit sachant que la langue est connue par le nom du fichier.
	// On constitue un tableau [Code ISO 3166-2] = liste des traductions ([langue] = nom).
	include_spip('inc/ezmashup_dataset_source');
	$traductions = [];
	foreach (['trad_fr'] as $_id_source) {
		$source = dataset_source_extraire($_id_source, $feed, $erreur);
		$langue = str_replace('trad_', '', $_id_source);
		foreach ($source as $_source) {
			$traductions[$_source['code_3166_2']][$langue] = $_source['trad'];
		}
	}

	// Ajout des traductions à l'index label de chaque enregistrement concerné
	if ($traductions) {
		include_spip('ezmashup/ezmashup');
		foreach ($enregistrements as $_cle => $_enregistrement) {
			$code_iso = $_enregistrement['code_3166_2'];
			if (isset($traductions[$code_iso])) {
				$enregistrements[$_cle]['label'] = ezmashup_record_completer_traduction(
					$_enregistrement['label'],
					$traductions[$code_iso]
				);
			}
		}
	}

	return $enregistrements;
}

/**
 * Complète la consigne de peuplement du feed `area_subdivisions`.
 *
 * @param array $consigne        Consigne initialisé par défaut par Mashup Factory
 * @param array $enregistrements Liste des enregistrement du dataset cible
 * @param array $feed            Configuration du feed
 *
 * @return array Consigne éventuellement mise à jour.
 */
function area_subdivisions_target_completer_consigne(array $consigne, array $enregistrements, array $feed) : array {
	// Ajouter à la consigne d'execution la liste des hash par pays possédant des subdivisions
	$consigne = consigne_ajouter_hash_pays($consigne, $enregistrements);

	// Vérifier la liste des pays inclus dans le tag du même nom en écrasant la valeur configurée
	$pays = array_unique(array_column($enregistrements, 'country'));
	$consigne['feed']['tags'] = $feed['tags']['pays'] = implode(',', $pays);

	return $consigne;
}

/**
 * Complète la consigne de peuplement du feed `area_protected_areas`.
 *
 * @param array $consigne        Consigne initialisé par défaut par Mashup Factory
 * @param array $enregistrements Liste des enregistrement du dataset cible
 * @param array $feed            Configuration du feed
 *
 * @return array Consigne éventuellement mise à jour.
 */
function area_protected_areas_target_completer_consigne(array $consigne, array $enregistrements, array $feed) : array {
	// Ajouter à la consigne d'execution la liste des hash par pays possédant des subdivisions
	return consigne_ajouter_hash_pays($consigne, $enregistrements);
}


// ----------------------------------------------------------------------------
// --------------- Utilitaires pour les traitements récurrents ----------------
// ----------------------------------------------------------------------------

/**
 * Complète la consigne de peuplement avec le hash par pays.
 *
 * @param array $consigne        Consigne initialisé par défaut par Mashup Factory
 * @param array $enregistrements Liste des enregistrement du dataset cible
 *
 * @return array Enregistrement complété.
 */
function consigne_ajouter_hash_pays(array $consigne, array $enregistrements) : array {
	// Ajouter à la consigne d'execution la liste des hash par pays possédant des subdivisions
	$enregistrements_par_pays = [];
	// -- ranger les enregistrements par pays
	foreach ($enregistrements as $_enregistrement) {
		$pays = $_enregistrement['country'];
		$enregistrements_par_pays[$pays][] = $_enregistrement;
	}
	// -- calculer le hash pour chaque pays
	foreach ($enregistrements_par_pays as $_pays => $_enregistrements) {
		$consigne['records']['hash_by_country'][$_pays] = sha1(json_encode($_enregistrements));
	}

	return $consigne;
}
