<?php
/**
 * Ce fichier contient l'ensemble des fonctions sp&cifiques des feeds de catégorie 'language'.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// ----------------------------------------------------------------------------
// -------------------------- Feed lang_iana_subtag ---------------------------
// ----------------------------------------------------------------------------

/**
 * Décodage spécifique de la source IANA des subtags RFC 5646.
 *
 * @param string $contenu   Chaine à décoder
 * @param array  $arguments Arguments de la fonction de décodage spécifique ou vide sinon
 * @param string $id_source Identifiant de la source en cours de décodage
 * @param array  $feed      Configuration complète du feed
 *
 * @return array Source décodée sous la forme d'une liste d'items
 */
function iana_5646subtags_decoder(string $contenu, array $arguments, string $id_source, array $feed) : array {
	$items = [];

	if ($contenu) {
		// On récupére donc un tableau des éléments à lire en utilisant la fonction explode
		$contenu = explode('%%', $contenu);

		// On décompose maintenant chaque bloc en élément d'un tableau
		foreach ($contenu as $_item) {
			if (preg_match_all('%^([a-z]+-*[a-z]*):\s+(.*)%im', $_item, $matches)) {
				// L'index 1 correspond à la liste des titres et l'index 2 à la liste des
				// valeurs correspondantes.
				// Il faut donc reconstruire un tableau associatif [nom] => valeur
				$item = [];
				foreach ($matches[1] as $_cle => $_titre) {
					$champ = trim($_titre);
					if (in_array($champ, $feed['mapping']['basic_fields'])) {
						if ($champ === 'Tag') {
							// Parfois le champ Subtag est remplacé par un champ Tag : on uniformise en Subtag.
							$champ = 'Subtag';
						}
						$item[$champ] = $matches[2][trim($_cle)] ?? '';
					}
				}

				// Ajouter l'item calculé à la liste
				if ($item) {
					$items[] = $item;
				}
			}
		}
	}

	return $items;
}

/**
 * Complète un enregistrement de base du feed `lang_iso639_families` à partir d'une source qui est lue
 * une fois et mise en stockage statique.
 *
 * La fonction ajoute :
 * - la traduction en français
 * - le symbole
 *
 *
 * @param array $enregistrement Enregistrement du dataset cible déjà rempli
 * @param array $feed           Configuration du feed
 *
 * @return array Enregistrement complété.
 *
 * @throws Exception
 */
function lang_iso639_families_record_completer(array $enregistrement, array $feed) : array {
	// On stocke la source décodée en statique de façon à ne pas la relire à chaque appel
	static $source = [];

	// Lecture du fichier CSV hiérarchies de langue
	$id_source = 'families-hierarchy';
	if (!$source) {
		include_spip('inc/ezmashup_dataset_source');
		$source = dataset_source_extraire($id_source, $feed, $erreur);
		// On indexe le tableau par le code ISO de la famille
		$source = array_column($source, null, 'Identifier');
	}

	$code = $enregistrement['code_639_5'];
	if (isset($source[$code])) {
		foreach ($feed['mapping']['addon_fields'][$id_source] as $_champ_target => $_champ_source) {
			// On initialise chaque champ
			$enregistrement[$_champ_target] = trim(table_valeur($source[$code], $_champ_source));
			// On traite les particularités des champs dont l'ordre de traitement est important
			if ($_champ_target === 'hierarchy') {
				$enregistrement[$_champ_target] = str_replace(' : ', ',', $enregistrement[$_champ_target]);
			} elseif ($_champ_target === 'parent') {
				$parents = explode(',', $enregistrement['hierarchy']);
				if (count($parents) > 1) {
					array_pop($parents);
					$enregistrement[$_champ_target] = array_pop($parents);
				}
			}
		}
	}

	return $enregistrement;
}
