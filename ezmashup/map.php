<?php
/**
 * Ce fichier contient l'ensemble des fonctions spécifuqes implémentant les feeds de catgéorie map.
 * Ces feeds sont des destinés à charger les différents ensemble de contours géographiques fournis sous des
 * formats variés (fichiers geoJSON, topoJSON, API REST...).
 *
 * @package SPIP\ISOCODE\SERVICES\MAP
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

require_once _DIR_PLUGIN_ISOCODE . 'vendor/autoload.php';
use Vicchi\GeoJson\Rewind;

// ----------------------------------------------------------------------------
// ---------- API du type de service MAP - Actions principales ----------
// ----------------------------------------------------------------------------

/**
 * Complète un enregistrement de la cible du feed `map_continents`.
 *
 * @param array $enregistrement Enregistrement du dataset cible déjà rempli
 * @param array $feed           Configuration du feed
 *
 * @return array Enregistrement complété.
 */
function map_continents_record_completer(array $enregistrement, array $feed) : array {
	// Apporter des corrections au champs déjà compilés : attention on a encore les index source !!!
	// - imposer le format Right-Hand Rule au contour
	// - encoder le champs des géométries
	$enregistrement['geometry'] = json_encode(Rewind::rewind($enregistrement['geometry'], true));

	return $enregistrement;
}

/**
 * Complète un item de la source du feed `map_countries`.
 *
 * @param array $item Item de la source
 * @param array $feed Configuration du feed
 *
 * @return array Item complété.
 */
function map_countries_item_completer(array $item, array $feed) : array {
	// Corriger une erreur dans le fichier où le code ISO vaut -99 : dans ce cas on va chercher le code ISO_A2_EH
	if (isset($item['properties']['ISO_A2'], $item['properties']['ISO_A2_EH'])) {
		if ($item['properties']['ISO_A2'] === '-99') {
			if ($item['properties']['ISO_A2_EH'] !== '-99') {
				$item['properties']['ISO_A2'] = $item['properties']['ISO_A2_EH'];
			} else {
				$item = [];
			}
		}
	} else {
		$item = [];
	}

	// Cette fonction permet d'apporter des corrections au champs déjà compilés : attention on a encore les index source !!!
	// - imposer le format Right-Hand Rule au contour
	// - encoder le champs des géométries
	if ($item) {
		if (strlen($item['properties']['ISO_A2']) === 2) {
			$item['geometry'] = json_encode(Rewind::rewind($item['geometry'], true));
		} else {
			$item = [];
		}
	}

	return $item;
}

/**
 * Complète un enregistrement de la cible du feed `map_fr_regions`.
 * - récupérer la latitude et la longitude à partir du champ geo_point_2d
 * - imposer le format Right-Hand Rule au contour
 * - encoder le champs des géométries.
 *
 * @param array $enregistrement Enregistrement du dataset cible déjà rempli
 * @param array $feed           Configuration du feed
 *
 * @return array Enregistrement complété.
 */
function map_fr_regions_record_completer(array $enregistrement, array $feed) : array {
	return opendatasoft_georef_record_completer($enregistrement);
}

/**
 * Complète un enregistrement de la cible du feed `map_fr_departments`.
 * - récupérer la latitude et la longitude à partir du champ geo_point_2d
 * - imposer le format Right-Hand Rule au contour
 * - encoder le champs des géométries.
 *
 * @param array $enregistrement Enregistrement du dataset cible déjà rempli
 * @param array $feed           Configuration du feed
 *
 * @return array Enregistrement complété.
 */
function map_fr_departments_record_completer(array $enregistrement, array $feed) : array {
	return opendatasoft_georef_record_completer($enregistrement);
}

/**
 * Complète un enregistrement de la cible du feed `map_de_lander`.
 * - récupérer la latitude et la longitude à partir du champ geo_point_2d
 * - imposer le format Right-Hand Rule au contour
 * - encoder le champs des géométries.
 *
 * @param array $enregistrement Enregistrement du dataset cible déjà rempli
 * @param array $feed           Configuration du feed
 *
 * @return array Enregistrement complété.
 */
function map_de_lander_record_completer(array $enregistrement, array $feed) : array {
	return opendatasoft_georef_record_completer($enregistrement);
}

/**
 * Complète un enregistrement de la cible du feed `map_ch_cantons`.
 * - récupérer la latitude et la longitude à partir du champ geo_point_2d
 * - imposer le format Right-Hand Rule au contour
 * - encoder le champs des géométries.
 *
 * @param array $enregistrement Enregistrement du dataset cible déjà rempli
 * @param array $feed           Configuration du feed
 *
 * @return array Enregistrement complété.
 */
function map_ch_cantons_record_completer(array $enregistrement, array $feed) : array {
	return opendatasoft_georef_record_completer($enregistrement);
}

/**
 * Complète un enregistrement de la cible du feed `map_be_regions`.
 * - récupérer la latitude et la longitude à partir du champ geo_point_2d
 * - imposer le format Right-Hand Rule au contour
 * - encoder le champs des géométries
 * - normaliser le code région.
 *
 * @param array $enregistrement Enregistrement du dataset cible déjà rempli
 * @param array $feed           Configuration du feed
 *
 * @return array Enregistrement complété.
 */
function map_be_regions_record_completer(array $enregistrement, array $feed) : array {
	// Latitude, longitude et contour
	$enregistrement = opendatasoft_georef_record_completer($enregistrement);

	// Formater le code INS sur 5 digits
	$enregistrement['code'] = str_pad($enregistrement['code'], 5, '0', STR_PAD_LEFT);

	return $enregistrement;
}

/**
 * Complète un enregistrement de la cible du feed `map_be_regions`.
 * - récupérer la latitude et la longitude à partir du champ geo_point_2d
 * - imposer le format Right-Hand Rule au contour
 * - encoder le champs des géométries
 * - normaliser le code région.
 *
 * @param array $enregistrement Enregistrement du dataset cible déjà rempli
 * @param array $feed           Configuration du feed
 *
 * @return array Enregistrement complété.
 */
function map_be_provinces_record_completer(array $enregistrement, array $feed) : array {
	// Latitude, longitude et contour
	$enregistrement = opendatasoft_georef_record_completer($enregistrement);

	// Formater le code INS sur 5 digits
	$enregistrement['code'] = str_pad($enregistrement['code'], 5, '0', STR_PAD_LEFT);

	return $enregistrement;
}

/**
 * Complète un enregistrement de la cible du feed `map_us_states`.
 * - récupérer la latitude et la longitude à partir du champ geo_point_2d
 * - imposer le format Right-Hand Rule au contour
 * - encoder le champs des géométries
 * - normaliser le code en lui ajoutant le code ISO des US.
 *
 * @param array $enregistrement Enregistrement du dataset cible déjà rempli
 * @param array $feed           Configuration du feed
 *
 * @return array Enregistrement complété.
 */
function map_us_states_record_completer(array $enregistrement, array $feed) : array {
	// Latitude, longitude et contour
	$enregistrement = opendatasoft_georef_record_completer($enregistrement);

	// Formater le code ISO en lui rajoutant le code ISO-3166 alpha2 des US
	$enregistrement['code'] = $enregistrement['country'] . '-' . $enregistrement['code'];

	return $enregistrement;
}

/**
 * Complète un enregistrement de la cible du feed `map_de_lander`.
 * - récupérer la latitude et la longitude à partir du champ geo_point_2d
 * - imposer le format Right-Hand Rule au contour
 * - encoder le champs des géométries.
 *
 * @param array $enregistrement Enregistrement du dataset cible déjà rempli
 * @param array $feed           Configuration du feed
 *
 * @return array Enregistrement complété.
 */
function map_nl_provinces_record_completer(array $enregistrement, array $feed) : array {
	return opendatasoft_georef_record_completer($enregistrement);
}

/**
 * Complète un enregistrement de la cible du feed `map_us_states`.
 * - récupérer la latitude et la longitude à partir du champ geo_point_2d
 * - imposer le format Right-Hand Rule au contour
 * - encoder le champs des géométries
 * - normaliser le code en lui ajoutant le code ISO du Portugal.
 *
 * @param array $enregistrement Enregistrement du dataset cible déjà rempli
 * @param array $feed           Configuration du feed
 *
 * @return array Enregistrement complété.
 */
function map_pt_districts_record_completer(array $enregistrement, array $feed) : array {
	// Latitude, longitude et contour
	$enregistrement = opendatasoft_georef_record_completer($enregistrement);

	// Formater le code ISO en lui rajoutant le code ISO-3166 alpha2 des US
	$enregistrement['code'] = $enregistrement['country'] . '-' . $enregistrement['code'];

	return $enregistrement;
}

/**
 * Complète un enregistrement de la cible du feed `pa_fr_marine_parks`.
 *
 * @param array $enregistrement Enregistrement du dataset cible déjà rempli
 * @param array $feed           Configuration du feed
 *
 * @return array Enregistrement complété.
 */
function pa_fr_marine_parks_record_completer(array $enregistrement, array $feed) : array {
	// Apporter des corrections au champs déjà compilés
	// - imposer le format Right-Hand Rule au contour
	// - encoder le champs des géométries

	return opendatasoft_georef_record_completer($enregistrement, false);
}

/**
 * Complète un enregistrement de la cible du feed `pa_fr_national_parks`.
 *
 * @param array $enregistrement Enregistrement du dataset cible déjà rempli
 * @param array $feed           Configuration du feed
 *
 * @return array Enregistrement complété.
 */
function pa_fr_national_parks_record_completer(array $enregistrement, array $feed) : array {
	// Apporter des corrections au champs déjà compilés
	// - imposer le format Right-Hand Rule au contour
	// - encoder le champs des géométries

	return opendatasoft_georef_record_completer($enregistrement, false);
}

/**
 * Complète un enregistrement de la cible du feed `pa_fr_regional_parks`.
 *
 * @param array $enregistrement Enregistrement du dataset cible déjà rempli
 * @param array $feed           Configuration du feed
 *
 * @return array Enregistrement complété.
 */
function pa_fr_regional_parks_record_completer(array $enregistrement, array $feed) : array {
	// Apporter des corrections au champs déjà compilés
	// - imposer le format Right-Hand Rule au contour
	// - encoder le champs des géométries

	return opendatasoft_georef_record_completer($enregistrement, false);
}

// ----------------------------------------------------------------------------
// ---------- Utilitaires pour les data OpenDataSoft ----------
// ----------------------------------------------------------------------------

/**
 * Complète un enregistrement de la cible d'un feed de map provenant d'Opendatasoft d'une façon standardisée:
 * - récupérer la latitude et la longitude à partir du champ geo_point_2d
 * - imposer le format Right-Hand Rule au contour
 * - encoder le champs des géométries.
 *
 * @param array $enregistrement Enregistrement du dataset cible déjà rempli
 * @param ?bool $avec_latlon
 *
 * @return array Enregistrement complété.
 */
function opendatasoft_georef_record_completer(array $enregistrement, ?bool $avec_latlon = true) : array {
	// Cette fonction permet de remplir les champs "basic_ext".
	// - récupérer la latitude et la longitude à partir du champ geo_point_2d
	if ($avec_latlon) {
		$enregistrement['lat'] = (float) ($enregistrement['lat_lon'][0]);
		$enregistrement['lon'] = (float) ($enregistrement['lat_lon'][1]);
	}

	// - imposer le format Right-Hand Rule au contour
	// - encoder le champs des géométries
	$enregistrement['geometry'] = json_encode(Rewind::rewind($enregistrement['geometry'], true));

	return $enregistrement;
}
