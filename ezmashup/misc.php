<?php
/**
 * Ce fichier contient l'ensemble des constantes et fonctions implémentant le type de service INFRASUBDIVISION.
 * Les services de INFRASUBDIVISION sont des mécanisme internes à Nomenclatures pour
 * charger les codes nationaux des infra subdivisions fournis par des services divers.
 *
 * @package SPIP\ISOCODE\SERVICES\MISC
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// -----------------------------------------------------------------------------------
// ---------- API du type de service INFRASUBDIVISION - Actions principales ----------
// -----------------------------------------------------------------------------------

/**
 * Complète un enregistrement de base du feed `misc_iso4217_currencies` à partir d'une source qui est lue
 * une fois et mise en stockage statique.
 *
 * La fonction ajoute :
 * - la traduction en français
 * - le symbole
 *
 *
 * @param array $enregistrement Enregistrement du dataset cible déjà rempli
 * @param array $feed           Configuration du feed
 *
 * @return array Enregistrement complété.
 *
 * @throws Exception
 */
function misc_iso4217_currencies_record_completer(array $enregistrement, array $feed) : array {
	// On stocke la source décodée en statique de façon à ne pas la relire à chaque appel
	static $source = [];

	// Lecture du fichier CSV des traductions françaises et des symboles.
	$id_source = 'currency_trad_symbol';
	if (!$source) {
		include_spip('inc/ezmashup_dataset_source');
		$source = dataset_source_extraire($id_source, $feed, $erreur);
		// On indexe le tableau par le code ISO 4217 alpha3
		$source = array_column($source, null, 'ISO');
	}

	// On rajoute les champs label_fr et symbole
	$code = $enregistrement['code_4217_3'];
	if (isset($source[$code])) {
		foreach ($feed['mapping']['addon_fields'][$id_source] as $_champ_target => $_champ_source) {
			$enregistrement[$_champ_target] = table_valeur($source[$code], $_champ_source);
			if (
				($_champ_target === 'symbol')
				and preg_match('/[^\s,]+/', $enregistrement[$_champ_target], $matches)
			) {
				$enregistrement[$_champ_target] = $matches[0];
			}
		}
	}

	return $enregistrement;
}
