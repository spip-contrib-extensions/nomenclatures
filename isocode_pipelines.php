<?php
/**
 * Ce fichier contient l'ensemble des fonctions implémentant l'API du plugin.
 *
 * @package SPIP\ISOCODE\COLLECTION
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Déclare les collections accessibles via l'API ezREST.
 * Par défaut, le plugin propose une liste de collections.
 *
 * @pipeline liste_ezcollection
 *
 * @param array $collections Configuration des collections déjà déclarées.
 *
 * @return array Collections complétées.
**/
function isocode_liste_ezcollection(array $collections) :array {
	// Initialisation du tableau des collections
	if (!$collections) {
		$collections = [];
	}

	$collections['continents'] = [
		'module' => 'isocode',
		'cache'  => [
			'type'  => 'ezrest',
			'duree' => 3600 * 24 * 30
		],
		'filtres' => []
	];

	$collections['zones'] = [
		'module' => 'isocode',
		'cache'  => [
			'type'  => 'ezrest',
			'duree' => 3600 * 24 * 30
		],
		'filtres' => []
	];

	$collections['pays'] = [
		'module' => 'isocode',
		'cache'  => [
			'type'  => 'ezrest',
			'duree' => 3600 * 24 * 30
		],
		'filtres' => [
			[
				'critere'         => 'zone',
				'est_obligatoire' => false,
				'champ_nom'       => 'code_num_region',
				'champ_table'     => 'iso3166countries',
				'format'          => '#^[0-9]{3}$#'
			],
			[
				'critere'         => 'continent',
				'est_obligatoire' => false,
				'champ_nom'       => 'code_continent',
				'champ_table'     => 'iso3166countries',
				'format'          => '#^[A-Z]{2}$#'
			],
		],
		'ressource' => 'code_alpha2'
	];

	$collections['subdivisions'] = [
		'module' => 'isocode',
		'cache'  => [
			'type'  => 'ezrest',
			'duree' => 3600 * 24 * 30
		],
		'filtres' => [
			[
				'critere'         => 'pays',
				'est_obligatoire' => false,
				'champ_nom'       => 'country',
				'champ_table'     => 'iso3166subdivisions'
			],
			[
				'critere'         => 'type',
				'est_obligatoire' => false,
				'champ_nom'       => 'type',
				'champ_table'     => 'iso3166subdivisions'
			],
			[
				'critere'         => 'exclure', // alternates, pays, subdivisions
				'est_obligatoire' => false,
				'sans_condition'  => true,
			],
		]
	];

	$collections['protected_areas'] = [
		'module' => 'isocode',
		'cache'  => [
			'type'  => 'ezrest',
			'duree' => 3600 * 24 * 30
		],
		'filtres' => [
			[
				'critere'         => 'pays',
				'est_obligatoire' => false,
				'champ_nom'       => 'country',
				'champ_table'     => 'wdpas'
			],
			[
				'critere'         => 'type',
				'est_obligatoire' => false,
				'champ_nom'       => 'type',
				'champ_table'     => 'wdpas'
			],
			[
				'critere'         => 'exclure', // alternates, pays, subdivisions
				'est_obligatoire' => false,
				'sans_condition'  => true,
			],
		]
	];

	$collections['infrasubdivisions'] = [
		'module' => 'isocode',
		'cache'  => [
			'type'  => 'ezrest',
			'duree' => 3600 * 24 * 30
		],
		'filtres' => [
			[
				'critere'         => 'service',
				'est_obligatoire' => false,
				'champ_nom'       => 'service',
				'champ_table'     => 'infrasubdivisions'
			],
			[
				'critere'         => 'pays',
				'est_obligatoire' => false,
				'champ_nom'       => 'country',
				'champ_table'     => 'infrasubdivisions'
			],
			[
				'critere'         => 'type',
				'est_obligatoire' => false,
				'champ_nom'       => 'type',
				'champ_table'     => 'infrasubdivisions'
			],
			[
				'critere'         => 'exclure', // alternates, pays, infrasubdivisions
				'est_obligatoire' => false,
				'sans_condition'  => true,
			],
		],
	];

	$collections['contours'] = [
		'module' => 'isocode',
		'cache'  => [
			'type'  => 'ezrest',
			'duree' => 3600 * 24 * 30
		],
		'filtres' => [
			[
				'critere'         => 'type',
				'est_obligatoire' => true,
				'champ_nom'       => 'type',
				'champ_table'     => 'geoboundaries'
			],
			[
				'critere'         => 'service',
				'est_obligatoire' => false,
				'champ_nom'       => 'service',
				'champ_table'     => 'geoboundaries'
			],
			[
				'critere'         => 'pays',
				'est_obligatoire' => false,
				'champ_nom'       => 'country',
				'champ_table'     => 'geoboundaries'
			],
			[
				'critere'         => 'exclure', // alternates
				'est_obligatoire' => false,
				'sans_condition'  => true,
			],
		],
	];

	return $collections;
}
