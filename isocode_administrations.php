<?php
/**
 * Fichier gérant l'installation et la désinstallation du plugin.
 *
 * @package    SPIP\ISOCODE\ADMINISTRATION
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction d'installation et de mise à jour du plugin.
 * Le schéma du plugin est composé des tables `spip_iso639xxxx` et d'une configuration.
 *
 * @param string $nom_meta_base_version Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible         Version du schéma de données (déclaré dans paquet.xml)
 *
 * @return void
 **/
function isocode_upgrade(string $nom_meta_base_version, string $version_cible) : void {
	$maj = [];

	// Liste des tables créées par le plugin
	$tables = [
		'spip_iso639codes',
		'spip_iso639names',
		'spip_iso639macros',
		'spip_iso639retirements',
		'spip_iso639families',
		'spip_iso15924scripts',
		'spip_iana5646subtags',
		'spip_iso4217currencies',
		'spip_geoipcontinents',
		'spip_m49regions',
		'spip_iso3166countries',
		'spip_iso3166subdivisions',
		'spip_wdpas',
		'spip_infrasubdivisions',
		'spip_iso3166alternates',
		'spip_geoboundaries',
	];

	$maj['create'] = [
		['maj_tables', $tables],
	];

	$maj['2'] = [
		['maj_tables', ['spip_infrasubdivisions']],
		['sql_alter', "TABLE spip_iso3166alternates CHANGE code_iso code_iso varchar(20) DEFAULT '' NOT NULL"],
		['sql_alter', "TABLE spip_iso3166alternates CHANGE code_alter code_alter varchar(20) DEFAULT '' NOT NULL"],
	];

	$maj['3'] = [
		['sql_alter', 'TABLE spip_m49regions ADD COLUMN depth int DEFAULT 0 NOT NULL AFTER parent'],
		['sql_alter', 'TABLE spip_iso3166subdivisions ADD COLUMN depth int DEFAULT 0 NOT NULL AFTER parent'],
		['sql_alter', 'TABLE spip_infrasubdivisions ADD COLUMN depth int DEFAULT 0 NOT NULL AFTER parent'],
	];

	$maj['4'] = [
		['sql_alter', 'TABLE spip_infrasubdivisions ADD COLUMN postcode varchar(10) DEFAULT "" NOT NULL AFTER label'],
		['sql_alter', 'TABLE spip_infrasubdivisions ADD COLUMN lat double NULL NULL AFTER postcode'],
		['sql_alter', 'TABLE spip_infrasubdivisions ADD COLUMN lon double NULL NULL AFTER lat'],
	];

	$maj['5'] = [
		['sql_alter', 'TABLE spip_infrasubdivisions ADD COLUMN parent_alt1 varchar(20) DEFAULT "" NOT NULL AFTER depth'],
		['sql_alter', 'TABLE spip_infrasubdivisions ADD COLUMN parent_alt2 varchar(20) DEFAULT "" NOT NULL AFTER parent_alt1'],
	];

	$maj['6'] = [
		['sql_alter', 'TABLE spip_m49regions MODIFY category varchar(64) DEFAULT "" NOT NULL'],
		['sql_alter', 'TABLE spip_iso3166countries MODIFY category varchar(64) DEFAULT "" NOT NULL'],
		['sql_alter', 'TABLE spip_iso3166subdivisions MODIFY type varchar(64) DEFAULT "" NOT NULL'],
		['sql_alter', 'TABLE spip_infrasubdivisions MODIFY type varchar(64) DEFAULT "" NOT NULL'],
	];

	$maj['7'] = [
		['sql_alter', 'TABLE spip_iso15924scripts DROP COLUMN label_en'],
		['sql_alter', 'TABLE spip_iso15924scripts DROP COLUMN label_fr'],
		['sql_alter', 'TABLE spip_iso4217currencies DROP COLUMN label_en'],
		['sql_alter', 'TABLE spip_iso4217currencies DROP COLUMN label_fr'],
		['sql_alter', 'TABLE spip_iso639families DROP COLUMN label_en'],
		['sql_alter', 'TABLE spip_iso639families DROP COLUMN label_fr'],
		['sql_alter', 'TABLE spip_iso639families DROP COLUMN code_639_1'],
		['effacer_meta', 'isocode']
	];

	$maj['8'] = [
		['maj_tables', ['spip_natureparks']],
	];

	$maj['9'] = [
		['sql_alter', 'TABLE spip_iso3166subdivisions ADD COLUMN creation_date datetime DEFAULT "0000-00-00 00:00:00" NOT NULL AFTER label'],
		['sql_alter', 'TABLE spip_iso3166subdivisions ADD COLUMN area int DEFAULT 0 NOT NULL AFTER label'],
		['sql_drop_table', 'spip_natureparks'],
	];

	$maj['10'] = [
		['sql_alter', 'TABLE spip_iso3166subdivisions MODIFY code_3166_2 varchar(16) DEFAULT "" NOT NULL'],
	];

	$maj['11'] = [
		['sql_alter', 'TABLE spip_iso639codes ADD COLUMN service varchar(30) DEFAULT "" NOT NULL AFTER code_639_1'],
		['sql_alter', 'TABLE spip_iso639names ADD COLUMN service varchar(30) DEFAULT "" NOT NULL AFTER code_639_3'],
		['sql_alter', 'TABLE spip_iso639macros ADD COLUMN service varchar(30) DEFAULT "" NOT NULL AFTER code_639_3'],
		['sql_alter', 'TABLE spip_iso639retirements ADD COLUMN service varchar(30) DEFAULT "" NOT NULL AFTER code_639_3'],
		['sql_alter', 'TABLE spip_iso639families ADD COLUMN service varchar(30) DEFAULT "" NOT NULL AFTER code_639_5'],
		['sql_alter', 'TABLE spip_iso15924scripts ADD COLUMN service varchar(30) DEFAULT "" NOT NULL AFTER code_15924'],
		['sql_alter', 'TABLE spip_iana5646subtags ADD COLUMN service varchar(30) DEFAULT "" NOT NULL AFTER subtag'],
		['sql_alter', 'TABLE spip_geoipcontinents ADD COLUMN service varchar(30) DEFAULT "" NOT NULL AFTER code_num'],
		['sql_alter', 'TABLE spip_m49regions ADD COLUMN service varchar(30) DEFAULT "" NOT NULL AFTER code_num'],
		['sql_alter', 'TABLE spip_iso3166countries ADD COLUMN service varchar(30) DEFAULT "" NOT NULL AFTER code_num'],
		['sql_alter', 'TABLE spip_iso3166subdivisions ADD COLUMN service varchar(30) DEFAULT "" NOT NULL AFTER code_3166_2'],
		['sql_alter', 'TABLE spip_iso3166alternates ADD COLUMN service varchar(30) DEFAULT "" NOT NULL AFTER code_alter'],
		['sql_alter', 'TABLE spip_iso4217currencies ADD COLUMN service varchar(30) DEFAULT "" NOT NULL AFTER code_num'],
		['maj_11_colonne_service']
	];

	$maj['12'] = [
		['maj_tables', ['spip_wdpas']],
		['sql_alter', 'TABLE spip_iso3166subdivisions DROP COLUMN area'],
		['sql_alter', 'TABLE spip_iso3166subdivisions DROP COLUMN creation_date'],
	];

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);

	// Recharger les feeds si une modification de la configuration a eu lieu.
	include_spip('inc/ezmashup_feed');
	feed_charger('isocode', true);
}

/**
 * Fonction de désinstallation du plugin.
 *
 * @param string $nom_meta_base_version Nom de la meta informant de la version du schéma de données du plugin installé
 *                                      dans SPIP.
 *
 * @throws Exception
 *
 * @return void
 */
function isocode_vider_tables(string $nom_meta_base_version) : void {
	// Répertorier les tables créées par le plugin (datasets cibles)
	include_spip('inc/isocode_feed');
	$tables = feed_repertorier_tables();

	// Décharger les feeds du plugin : vider les tables, supprimer les consignations, vider le stockage des feeds
	// (les tables ne sont pas supprimées)
	include_spip('inc/ezmashup_feed');
	feed_decharger('isocode');

	// Supprimer les tables créées par le plugin (datasets cibles) maintenant qu'elles sont vides
	foreach ($tables as $_table) {
		sql_drop_table($_table);
	}

	// Effacer la meta de stockage des feeds du plugin maintenant qu'elle est vide
	effacer_meta('isocode_mashup');

	// Effacer la meta du schéma de la base
	effacer_meta($nom_meta_base_version);
}

/**
 * Insertion du feed dans la nouvelle colonne service pour les feeds déjà chargés.
 *
 * @return void
 */
function maj_11_colonne_service() {
	$tables = [
		'spip_iso639codes'         => 'lang_iso639_codes',
		'spip_iso639names'         => 'lang_iso639_names',
		'spip_iso639macros'        => 'lang_iso639_macros',
		'spip_iso639retirements'   => 'lang_iso639_retirements',
		'spip_iso639families'      => 'lang_iso639_families',
		'spip_iso15924scripts'     => 'lang_iso15924_scripts',
		'spip_iana5646subtags'     => 'lang_iana_subtags',
		'spip_iso4217currencies'   => 'misc_iso4217_currencies',
		'spip_geoipcontinents'     => 'area_continents',
		'spip_m49regions'          => 'area_regions',
		'spip_iso3166countries'    => 'area_countries',
		'spip_iso3166subdivisions' => 'area_subdivisions',
		'spip_iso3166alternates'   => 'area_alternates',
	];

	foreach ($tables as $_table => $_id_feed) {
		sql_updateq($_table, ['service' => $_id_feed]);
	}
}
